### Endpoints

## Login

* Endpoint path: /token
* Endpoint method: POST

* Request shape:
    * username: string
    * password: string

* Response: Account info and bearer token
* Response shape (JSON):
```
{
    "account": {
        key: type,
    },
    "token": string
}
```

## Logout

* Endpoint path: /token
* Endpoint method: DELETE

* Headers:
    * Authorization: Bearer token

* Response: true
* Response shape (JSON):
```
true
```

## Create User
* Endpoint path: /createuser
* Endpoint method: POST

* Response true
* Response shape (JSON):
```
{
    "first_name": "string",
    "last_name": "string",
    "username": "string',
    "email": "string",
    "password": "string",
    "password_confirmation": "string"
}
```

## Update user
* Endpoint path: /updateuser
* Endpoint method: PUT

* Headers:
    * Authorization: Bearer token

* Reponse true
* Response shape (JSON):
```
{
    "first_name": "string",
    "last_name": "string",
    "username": "string',
    "email": "string",
    "password": "string",
    "password_confirmation": "string"
}
```




## Game Statistics
* Endpoint path: /stats/units
* Endpoint method: GET

* Response: true
* Response shape:
```
{
    "unit_type": "name",
    "damage_dealt": "int",
    "kills": "int",
    "deaths" "int"
}
```

## Player Statistics
* Endpoint path: /stats/{player_id}
* Endpoint method: GET

* Respones: true
* Response shape:
```
{
    "username": "string",
    "games_played: "int",
    "win_loss_ratio: "numeric",
    "units_generated: "int",
    "units_destroyed: "int"
}
```
