DROP TABLE IF EXISTS tile_archtype;
DROP TABLE IF EXISTS actions;

CREATE TABLE tile_archtype (
    id SERIAL NOT NULL UNIQUE,
    name VARCHAR(50) NOT NULL,
    type VARCHAR(50) NOT NULL,
    size VARCHAR(50) NOT NULL,
    sprite VARCHAR(256),
    detection_range SMALLINT
);

CREATE TABLE actions (
    id SERIAL NOT NULL UNIQUE,
    owner VARCHAR(50) NOT NULL,
    action VARCHAR(200) NOT NULL,
    action_type VARCHAR(200) NOT NULL,
    action_cost VARCHAR(50) NOT NULL
);
