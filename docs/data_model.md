# Data Model

### User:

| field           | type   | optional? | unique? |
| --------------- | ------ | --------- | ------- |
| user_id         | serial | no        | yes     |
| first_name      | string | no        | no      |
| last_name       | string | no        | no      |
| username        | string | no        | yes     |
| email           | string | no        | yes     |
| hashed_password | string | no        | no      |

- The user's password is converted to and then stored in our database as a hashed password, but on the user's end it is just a regular password.

### Field Archetypes:

| field           | type   | optional? | unique? |
| --------------- | ------ | --------- | ------- |
| field_id        | int    | no        | yes     |
| name            | string | yes       | yes     |
| type            | string | yes       | no      |
| cost            | int    | yes       | no      |
| max_health      | int    | yes       | yes     |
| sprite          | json   | yes       | no      |
| detection_range | int    | yes       | no      |
| tile_shape      | string | yes       | no      |

### Movement:

| field           | type                              | optional? | unique? |
| --------------- | --------------------------------- | --------- | ------- |
| movement_id     | int                               | no        | yes     |
| unit_name       | refernce to field_archetypes name | yes       | no      |
| travel          | int                               | yes       | no      |
| travel_cost     | int                               | yes       | no      |
| march           | int                               | yes       | no      |
| march_cost      | int                               | yes       | no      |
| sneak           | int                               | yes       | no      |
| sneak_cost      | string                            | yes       | no      |
| sneak_cost      | string                            | yes       | no      |

### Abilities:

| field             | type                              | optional? | unique? |
| ----------------- | --------------------------------- | --------- | ------- |
| ability_id        | int                               | no        | yes     |
| unit_name         | refernce to field_archetypes name | yes       | no      |
| ability_one       | string                            | yes       | no      |
| ability_one_range | int                               | yes       | no      |
| ability_one_cost  | int                               | yes       | no      |
| ability_two       | string                            | yes       | no      |
| ability_two_range | int                               | yes       | no      |
| ability_two_cost  | string                            | yes       | no      |

### Game:

| field       | type                              | optional? | unique? |
| ----------- | --------------------------------- | --------- | ------- |
| port        | string                            | no        | yes     |
| created_on  | date                              | no        | no      |
| total_turns | in                                | no        | no      |
| is_active   | boolean                           | no        | no      |
| player_count| int                               | no        | no      |
| host        | reference to users username       | no        | no      |
| player_1    | reference to users username       | no        | no      |
| player_2    | reference to users username       | yes       | no      |
| player_3    | reference to users username       | yes       | no      |
| player_4    | reference to users username       | yes       | no      |
| player_1_int| int                               | yes       | no      |
| player_2_int| int                               | yes       | no      |
| player_3_int| int                               | yes       | no      |
| player_4_int| int                               | yes       | no      |

### Floor Archetypes:

| field           | type   | optional? | unique? |
| --------------- | ------ | --------- | ------- |
| terrain_id      | serial | no        | yes     |
| name            | string | no        | no      |
| type            | string | no        | no      |
| sprite          | text   | yes       | no      |

### Floor Tiles:

| field        | type                         | optional? | unique? |
| ------------ | ---------------------------- | --------- | ------- |
| tile_id      | serial                       | no        | yes     |
| game_port    | refernce to games port       | no        | no      |
| archetype    | refernce to floor_archetypes | no        | no      |
| position     | text                         | yes       | no      |

### Field Tiles:

| field        | type                         | optional? | unique? |
| ------------ | ---------------------------- | --------- | ------- |
| tile_id      | serial                       | no        | yes     |
| game_port    | refernce to games            | no        | no      |
| archetype    | refernce to floor_archetypes | no        | no      |
| position     | string                       | no        | no      |
| owner        | int                          | no        | no      |
| status       | string                       | no        | no      |
| visibility   | json                         | no        | no      |
| health       | int                          | yes       | no      |

### Shade Tiles:

| field           | type                | optional? | unique? |
| --------------- | ------------------- | --------- | ------- |
| tile_id         | serial              | no        | yes     |
| game_port       | refernce to games   | no        | no      |
| position        | string              | no        | no      |
| visibility      | string              | no        | no      |
| owner           | int                 | yes       | no      |
| turnWhenHidden  | int                 | yes       | no      |
| color           | string              | yes       | no      |
