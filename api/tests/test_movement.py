from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator
from queries.tiles import Archetype_Repo, Movement
from pydantic import BaseModel


client = TestClient(app)


class UserOut(BaseModel):
    username: str
    email: str


class MovementTesting:
    data: dict


moves = {}


class FakeMovement:
    def get_moves(*args):
        return Movement(data={})

    def fake_get_current_account_data():
        account = True
        return account


def test_get_moves():

    # Arrange
    app.dependency_overrides[authenticator.get_current_account_data] = FakeMovement.fake_get_current_account_data

    app.dependency_overrides[Archetype_Repo] = FakeMovement

    # Act
    response = client.get("/movement")

    # Clean up
    app.dependency_overrides = {}

    print(response.json())

    # Assert
    assert response.status_code == 200
    assert response.json() == FakeMovement.get_moves()
