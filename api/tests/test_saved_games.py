from fastapi.testclient import TestClient
from authenticator import authenticator
from main import app
from queries.saved_games import GameRepo, GameMapOut


client = TestClient(app)


class FakeRepo:
    def get_map_data(self, port):
        return GameMapOut(floor_tiles={}, field_tiles={}, shade_tiles={})


def fake_auth():
    return True


def test_get_map_data():
    app.dependency_overrides[authenticator.get_current_account_data] = fake_auth
    app.dependency_overrides[GameRepo] = FakeRepo

    response = client.get("/api/game/get/map/XCLT")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == GameMapOut(floor_tiles={}, field_tiles={}, shade_tiles={})
