from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator
from queries.tiles import AbilitiesRepo, AllAbilities

client = TestClient(app)


class EmptyAbilitiesQueries:
    def getAllAbilities(*args):
        return AllAbilities(data={})


def fake_auth():
    return True


def test_getAllAbilities():
    app.dependency_overrides[AbilitiesRepo] = EmptyAbilitiesQueries
    app.dependency_overrides[authenticator.get_current_account_data] = fake_auth

    response = client.get("/abilities")

    app.dependency_overrides = {}
    print(response)
    assert response.status_code == 200

    assert response.json() == EmptyAbilitiesQueries.getAllAbilities()
