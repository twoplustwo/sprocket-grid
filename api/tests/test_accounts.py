from fastapi.testclient import TestClient
from main import app
from queries.accounts import AccountQueries
from authenticator import authenticator

client = TestClient(app)


class EmptyAccountQueries:
    def get_all_accounts(self):
        return []


def fake_auth():
    return True


def test_get_all_accounts():
    app.dependency_overrides[AccountQueries] = EmptyAccountQueries
    app.dependency_overrides[authenticator.get_current_account_data] = fake_auth
    response = client.get("/api/users")

    app.dependency_overrides = {}

    print(response.json())
    assert response.status_code == 200
    assert response.json() == []
