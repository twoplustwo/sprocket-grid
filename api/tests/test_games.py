from fastapi.testclient import TestClient
from main import app
from queries.saved_games import GameRepo
from authenticator import authenticator

client = TestClient(app)


class EmptyActiveGame:
    def get_active_games(self):
        return {
            "active_games": []
        }


def fake_auth():
    return True


def test_empty_active_games():
    app.dependency_overrides[GameRepo] = EmptyActiveGame
    app.dependency_overrides[authenticator.get_current_account_data] = fake_auth
    response = client.get("/api/games")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == {"active_games": []}
