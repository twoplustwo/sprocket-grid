async def migrate():
    from . import down, up, LATEST, ZERO
    import os
    import sys

    db_url = os.environ["DATABASE_URL"]

    if len(sys.argv) < 2:
        print("Command: up|down [amount]")
        exit(1)
    direction = sys.argv[1]
    amount = sys.argv[2] if len(sys.argv) > 2 else None
    if direction == "up":
        if amount is None:
            amount = LATEST
        else:
            try:
                amount = int(amount)
            except ValueError:
                print(f"Unknown amount {amount}")
        await up(db_url, to=amount)
    elif direction == "down":
        if amount is None:
            amount = 1
        elif amount == "zero":
            amount = ZERO
        else:
            try:
                amount = int(amount)
            except ValueError:
                print(f"Unknown amount {amount}")
        await down(db_url, to=amount)


async def delete_data():
    from queries.pool import pool

    try:
        with pool.connection() as conn:
            with conn.cursor() as db:

                db.execute(
                    """
                    UPDATE field_archetypes
                    SET name = NULL,
                        type = NULL,
                        cost = NULL,
                        max_health = NULL,
                        sprite = NULL,
                        detection_range = NULL,
                        tile_shape = NULL;
                    """
                )

                db.execute(
                    """
                    UPDATE movement
                    SET travel = NULL,
                        travel_cost = NULL,
                        march = NULL,
                        march_cost = NULL,
                        sneak = NULL,
                        sneak_cost = NULL;
                    """
                )

                db.execute(
                    """
                    UPDATE abilities
                    SET ability_one = NULL,
                        ability_one_range = NULL,
                        ability_one_cost = NULL,
                        ability_two = NULL,
                        ability_two_range = NULL,
                        ability_two_cost = NULL;
                    """
                )

    except Exception as error:
        print(error)
        return {"message": "failed to delete old data"}


async def init_db():
    from queries.pool import pool
    import json

    # Load data from the JSON file
    with open('stats.json') as f:
        data = json.load(f)

    try:
        with pool.connection() as conn:
            with conn.cursor() as db:
                # Insert or update data in the "field_archetypes" table
                for at in data.get("field_archetypes"):
                    db.execute(
                        """
                        INSERT INTO field_archetypes (field_id, name, type, cost, max_health, sprite, detection_range, tile_shape)
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
                        ON CONFLICT (field_id) DO UPDATE
                        SET name = EXCLUDED.name,
                            type = EXCLUDED.type,
                            cost = EXCLUDED.cost,
                            max_health = EXCLUDED.max_health,
                            sprite = EXCLUDED.sprite,
                            detection_range = EXCLUDED.detection_range,
                            tile_shape = EXCLUDED.tile_shape;
                        """,
                        [
                            at["field_id"],
                            at["name"],
                            at["type"],
                            at["cost"],
                            at["max_health"],
                            json.dumps(at["sprite"]),
                            at["detection_range"],
                            at["tile_shape"]
                        ]
                    )

                # Insert data into the "movement" table
                for at in data.get("movement"):
                    db.execute(
                        """
                        INSERT INTO movement (movement_id, unit_name, travel, travel_cost, march, march_cost, sneak, sneak_cost)
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
                        ON CONFLICT (movement_id) DO UPDATE
                        SET unit_name = EXCLUDED.unit_name,
                            travel = EXCLUDED.travel,
                            travel_cost = EXCLUDED.travel_cost,
                            march = EXCLUDED.march,
                            march_cost = EXCLUDED.march_cost,
                            sneak = EXCLUDED.sneak,
                            sneak_cost = EXCLUDED.sneak_cost;
                        """,
                        [
                            at["movement_id"],
                            at["unit_name"],
                            at["travel"],
                            at["travel_cost"],
                            at["march"],
                            at["march_cost"],
                            at["sneak"],
                            at["sneak_cost"]
                        ]
                    )

                # Insert data into the "abilities" table
                for at in data.get("abilities"):
                    db.execute(
                        """
                        INSERT INTO abilities (ability_id, unit_name, ability_one, ability_one_range, ability_one_cost, ability_two, ability_two_range, ability_two_cost)
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
                        ON CONFLICT (ability_id) DO UPDATE
                        SET unit_name = EXCLUDED.unit_name,
                            ability_one = EXCLUDED.ability_one,
                            ability_one_range = EXCLUDED.ability_one_range,
                            ability_one_cost = EXCLUDED.ability_one_cost,
                            ability_two = EXCLUDED.ability_two,
                            ability_two_range = EXCLUDED.ability_two_range,
                            ability_two_cost = EXCLUDED.ability_two_cost;
                        """,
                        [
                            at["ability_id"],
                            at["unit_name"],
                            at["ability_one"],
                            at["ability_one_range"],
                            at["ability_one_cost"],
                            at["ability_two"],
                            at["ability_two_range"],
                            at["ability_two_cost"]
                        ]
                    )

    except Exception as error:
        print(error)
        return {"message": "failed to initialize database"}


async def main():
    from queries.pool import pool
    from asyncio import run, create_task

    # fire migrate function
    await create_task(migrate())
    # delete data in database
    await create_task(delete_data())
    # fire init function
    await create_task(init_db())


if __name__ == "__main__":
    from asyncio import run

    run(main())
