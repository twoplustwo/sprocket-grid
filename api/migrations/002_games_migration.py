steps = [
    [
        """
        CREATE TABLE games (
            port VARCHAR(4) NOT NULL UNIQUE PRIMARY KEY,
            created_on DATE NOT NULL,
            total_turns SMALLINT NOT NULL,
            current_turn SMALLINT NOT NULL,
            is_active BOOLEAN DEFAULT TRUE NOT NULL,
            player_count SMALLINT NOT NULL,
            host VARCHAR(50) NOT NULL REFERENCES users(username),
            player_1 VARCHAR(50) NOT NULL REFERENCES users(username),
            player_2 VARCHAR(50) REFERENCES users(username),
            player_3 VARCHAR(50) REFERENCES users(username),
            player_4 VARCHAR(50) REFERENCES users(username),
            player_1_int SMALLINT,
            player_2_int SMALLINT,
            player_3_int SMALLINT,
            player_4_int SMALLINT

        );
        """,
        """
        DROP TABLE games;
        """
    ],
    [
        """
        CREATE TABLE floor_tiles (
            tile_id SERIAL NOT NULL PRIMARY KEY,
            game_port VARCHAR(4) NOT NULL REFERENCES games(port),
            archetype SMALLINT NOT NULL REFERENCES floor_archetypes,
            position VARCHAR(10) NOT NULL

        );
        """,
        """
        DROP TABLE floor_tiles;
        """
    ],
    [
        """
        CREATE TABLE field_tiles (
            tile_id SERIAL NOT NULL PRIMARY KEY,
            game_port VARCHAR(4) NOT NULL REFERENCES games,
            archetype SMALLINT NOT NULL REFERENCES field_archetypes,
            position VARCHAR(10) NOT NULL,
            owner SMALLINT NOT NULL,
            status VARCHAR(50) NOT NULL,
            visibility JSON NOT NULL,
            health SMALLINT
        );
        """,
        """
        DROP TABLE field_tiles;
        """
    ],
    [
        """
        CREATE TABLE shade_tiles (
            tile_id SERIAL NOT NULL PRIMARY KEY,
            game_port VARCHAR(4) NOT NULL REFERENCES games,
            position VARCHAR(10) NOT NULL,
            visibility VARCHAR(50) NOT NULL,
            owner SMALLINT,
            turnWhenHidden SMALLINT,
            color VARCHAR(16)
        );
        """,
        """
        DROP TABLE shade_tiles;
        """
    ]
]
