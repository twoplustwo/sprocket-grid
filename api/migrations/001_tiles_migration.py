steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE users (
            user_id SERIAL NOT NULL PRIMARY KEY,
            first_name varchar(50) NOT NULL,
            last_name varchar(50) NOT NULL,
            username varchar(100) NOT NULL unique,
            email varchar(50) NOT NULL unique,
            hashed_password varchar(200) NOT NULL
            );
        """,
        # "Down" SQL statement
        """
        DROP TABLE users;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE field_archetypes (
            field_id SMALLINT NOT NULL PRIMARY KEY,
            name VARCHAR(50) UNIQUE,
            type VARCHAR(50),
            cost SMALLINT,
            max_health SMALLINT,
            sprite JSON,
            detection_range SMALLINT,
            tile_shape TEXT
            );
        """,
        # "Down" SQL statement
        """
        DROP TABLE field_archetypes;
        """
    ],
    [
        # "Up" SQL statement
        # Create this data after the field_archetype data is done
        """
        CREATE TABLE movement (
            movement_id SMALLINT NOT NULL PRIMARY KEY,
            unit_name varchar(50),
            travel SMALLINT,
            travel_cost SMALLINT,
            march SMALLINT,
            march_cost SMALLINT,
            sneak SMALLINT,
            sneak_cost SMALLINT,
            FOREIGN KEY (unit_name) REFERENCES field_archetypes(name)
            );
        """,
        # "Down" SQL statement
        """
        DROP TABLE movement;
        """
    ],
    [
        # "Up" SQL statement
        # Create this data after the field_archetype data is done
        """
        CREATE TABLE abilities (
            ability_id SMALLINT NOT NULL PRIMARY KEY,
            unit_name VARCHAR(50),
            ability_one VARCHAR(250),
            ability_one_range SMALLINT,
            ability_one_cost SMALLINT,
            ability_two VARCHAR(250),
            ability_two_range SMALLINT,
            ability_two_cost SMALLINT,
            FOREIGN KEY (unit_name) REFERENCES field_archetypes(name)
            );
        """,
        # "Down" SQL statement
        """
        DROP TABLE abilities;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE floor_archetypes (
            terrain_id SERIAL NOT NULL PRIMARY KEY,
            name VARCHAR(50) NOT NULL,
            type VARCHAR(50) NOT NULL,
            sprite TEXT
            );
        """,
        # "Down" SQL statement
        """
        DROP TABLE floor_archetypes;
        """
    ]
]
