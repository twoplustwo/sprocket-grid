steps = [
    [
        """
        INSERT INTO floor_archetypes
            (name, type)
        VALUES
            ('default', 'default');
        """,
        """
        DELETE FROM floor_archetypes
        WHERE name = 'default';
        """
    ]
]
