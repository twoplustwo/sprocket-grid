import string
import random
from queries.pool import pool


def value_in_column(value, table: str, col: str):
    """
    Checks if given value is unique to a given table column.

    value exists in table column -> True

    value does not exist in table column -> False
    """
    try:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    f"""
                    SELECT {col}
                    FROM {table}
                    """
                )

                list_result = [item[0] for item in result.fetchall()]

                if value in list_result:
                    return True
                return False

    except Exception as error:
        print(error)
        return {"message": "failed to connect to database"}


def generate_port():
    """
    Generates a 4 character string using digits and uppercase letters

    Ensures string does not already exist in the "port" column of the "games" table and would be unique

    Returns:
        String: Example -> "D42L"
    """
    result = "".join(random.choices(string.ascii_uppercase + string.digits, k=4))
    is_unique = value_in_column(value=result, table="games", col="port")

    if not is_unique:
        return result

    generate_port()


def generate_map_tiles(port: str, player_count: int, db):
    """
    Params:
        port - 4 character port string of existing game

        player_count - number of players in game (1, 3, or 4, does not accept 2)

        db - The db variable of the parent query function

    Generates a matrix of floor tiles and shading tiles assigned to a given game entry.
    Coordinates of the generated tiles are determined by player count.

    DOES NOT ACCEPT A PLAYER COUNT OF 2!
    Map size is the same for 1 or 2 players, 3 columns are added per additional player past 2, up to 4.
    """
    range_options = {
        1: [0, 12],
        3: [12, 15],
        4: [15, 18]
    }
    ran_choice = range_options[player_count]

    try:
        for row in range(18):
            for col in range(ran_choice[0], ran_choice[1]):
                db.execute(
                    """
                    INSERT INTO floor_tiles
                        (
                            game_port,
                            archetype,
                            position
                        )
                    VALUES
                        (%s, %s, %s);
                    """,
                    [
                        port,
                        1,
                        f"{col},{row}"
                    ]
                )

                db.execute(
                    """
                    INSERT INTO shade_tiles
                        (
                            game_port,
                            position,
                            visibility
                        )
                    VALUES
                        (%s, %s, %s);
                    """,
                    [
                        port,
                        f"{col},{row}",
                        "hidden"
                    ]
                )
    except Exception as error:
        print(error)
        return {"message": f"{error}: Unable to generate tiles"}
