from datetime import date
import json
from typing import Optional, Union, List
from pydantic import BaseModel
from queries.pool import pool
from helpers import generate_port, generate_map_tiles, value_in_column
from psycopg.rows import dict_row
from fastapi.exceptions import HTTPException
from fastapi.logger import logger


class GameOutError(BaseModel):
    message: str
    status: Optional[int]


class JoinGameError(BaseModel):
    message: str
    status: int


class GameOut(BaseModel):
    username: Optional[str]
    port: str
    player_num: int
    initiative: int


class Player(BaseModel):
    username: Optional[str]
    port: Optional[str]


class GameDataIn(BaseModel):
    port: str
    total_turns: Optional[int]
    current_turn: Optional[int]
    player_1_int: Optional[int]
    player_2_int: Optional[int]
    player_3_int: Optional[int]
    player_4_int: Optional[int]
    floor_tiles: Optional[dict]
    field_tiles: Optional[dict]
    shade_tiles: Optional[dict]


class GameDataOut(BaseModel):
    total_turns: int
    current_turn: int
    player_count: int
    host: str
    player_1: Optional[str]
    player_2: Optional[str]
    player_3: Optional[str]
    player_4: Optional[str]
    player_1_int: Optional[int]
    player_2_int: Optional[int]
    player_3_int: Optional[int]
    player_4_int: Optional[int]


class GameMapOut(BaseModel):
    floor_tiles: dict
    field_tiles: dict
    shade_tiles: dict


class SaveSuccess(BaseModel):
    message: str
    status: int


class SaveFailure(BaseModel):
    message: str
    status: int


class ActiveGames(BaseModel):
    port: str
    player_count: int
    total_turns: int
    players: list


class ActiveGamesOut(BaseModel):
    active_games: List[ActiveGames]


class GameRepo:
    """
    Repository for game CRUD methods

    Methods:
        .create(player: Player): Initializes and creates a new game, adds querying player to new game, returns individual player data

        .join_game(player: Player): adds player to game if they're not already assigned, returns individual player data

        .get_floor_data(port):

        .get_field_data(port):

        .get_shading_data(port):

        .get_game_data(port):

        .get_map_data(port)

        .save_game(port):

        .update_field_data(port, data): deletes all field tiles from a game, then repopulates with the values in data

        .update_shade_data(port, data):

        .update_meta_data(port, data):

        .save_game(data: GameDataIn):

        .forfeit_game():

        .exit_game():

        .get_active_games():

    """

    def create(self, player: Player) -> GameOut:
        """
        Creates a new entry in the "games" table,
        generates a unique port string,
        sets creation date to current date,
        total turns to 0, current_player to 1,
        and player_1 to the user who sent the query.

        Generates a list of terrain and shading tiles associated to the new game and initialized for a 2 player map size.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    port = generate_port()
                    created_on = date.today()
                    total_turns = 0
                    player_turn = 1

                    result = db.execute(
                        """
                        INSERT INTO games
                            (
                                port,
                                created_on,
                                total_turns,
                                current_turn,
                                is_active,
                                player_count,
                                host,
                                player_1,
                                player_1_int
                            )
                        VALUES
                            (%s, %s, %s, %s, %s, %s, %s, %s, %s)
                        RETURNING port, player_1;
                        """,
                        [
                            port,
                            created_on,
                            total_turns,
                            player_turn,
                            True,
                            1,
                            player.username,
                            player.username,
                            10
                        ]
                    )
                    record = result.fetchone()

                    # Creates all floor and shade tiles for a 2 player map
                    generate_map_tiles(port=port, player_count=1, db=db)

                    if record is None:
                        return None

                    return GameOut(
                            port=port,
                            player_num=1,
                            initiative=10,
                    )

        except Exception as error:
            print(error)
            return {"message": "failed to create new game"}

    def join_game(self, player: Player) -> GameOut:
        """
        Manages players joining a game, accepts their username and a game port, returns required game data.
        If the player is not already assigned to a player slot they're assigned to the next open slot.
        On player assignment if the number of players > 2, additional map tiles are generated.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:

                    if not value_in_column(value=player.username, table="users", col="username"):
                        return JoinGameError(
                            message="Username does not exist",
                            status=400
                            )

                    if not value_in_column(value=player.port, table="games", col="port"):
                        return JoinGameError(
                            message="Invalid Game Code!",
                            status=404
                            )

                    result = db.execute(
                        """
                        SELECT player_1, player_2, player_3, player_4
                        FROM games
                        WHERE port = %s;
                        """,
                        [player.port]
                    )

                    all_players = [p for p in result.fetchone() if p is not None]
                    player_count = len(all_players)

                    if player.username not in all_players:
                        if player_count < 4:
                            sql_options = {
                                1:
                                    """
                                    UPDATE games
                                    SET player_2 = %s,
                                        player_count = 2,
                                        player_2_int = 10
                                    WHERE port = %s;
                                    """,
                                2:
                                    """
                                    UPDATE games
                                    SET player_3 = %s,
                                        player_count = 3,
                                        player_3_int = 10
                                    WHERE port = %s;
                                    """,
                                3:
                                    """
                                    UPDATE games
                                    SET player_4 = %s,
                                        player_count = 4,
                                        player_4_int = 10
                                    WHERE port = %s;
                                    """
                            }

                            db.execute(sql_options[player_count], [player.username, player.port])

                            # adds additional map columns if player count > 2
                            if player_count + 1 > 2:
                                generate_map_tiles(port=player.port, player_count=player_count+1, db=db)

                            playerList = db.execute(
                                """
                                SELECT player_1, player_2, player_3, player_4
                                FROM games
                                WHERE port = %s;
                                """,
                                [player.port]
                            )

                            record = playerList.fetchone()
                            player_num = record.index(player.username) + 1

                            dataOptions = {
                                1:
                                    """
                                    SELECT player_1_int
                                    FROM games
                                    WHERE port = %s;
                                    """,
                                2:
                                    """
                                    SELECT player_2_int
                                    FROM games
                                    WHERE port = %s;
                                    """,
                                3:
                                    """
                                    SELECT player_3_int
                                    FROM games
                                    WHERE port = %s;
                                    """,
                                4:
                                    """
                                    SELECT player_4_int
                                    FROM games
                                    WHERE port = %s;
                                    """,
                            }

                            initiativeQ = db.execute(dataOptions[player_num], [player.port])

                            return GameOut(
                                port=player.port,
                                player_num=player_num,
                                initiative=initiativeQ.fetchone()[0],
                            )

                        else:
                            return JoinGameError(
                                message=f"Game at port: {player.port} full!",
                            )

                    else:
                        playerList = db.execute(
                                """
                                SELECT player_1, player_2, player_3, player_4
                                FROM games
                                WHERE port = %s;
                                """,
                                [player.port]
                            )

                        record = playerList.fetchone()
                        player_num = record.index(player.username) + 1

                        dataOptions = {
                            1:
                                """
                                SELECT player_1_int
                                FROM games
                                WHERE port = %s;
                                """,
                            2:
                                """
                                SELECT player_2_int
                                FROM games
                                WHERE port = %s;
                                """,
                            3:
                                """
                                SELECT player_3_int
                                FROM games
                                WHERE port = %s;
                                """,
                            4:
                                """
                                SELECT player_4_int
                                FROM games
                                WHERE port = %s;
                                """,
                        }

                        initiativeQ = db.execute(dataOptions[player_num], [player.port])

                        return GameOut(
                            port=player.port,
                            player_num=player_num,
                            initiative=initiativeQ.fetchone()[0],
                        )

        except Exception as error:
            print(error)

    def get_floor_data(self, port):
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    result = db.execute(
                        """
                        SELECT archetype, position
                        FROM floor_tiles
                        WHERE game_port = %s;
                        """,
                        [port]
                    )
                    record = result.fetchall()
                    response = {}
                    for item in record:
                        response[item['position']] = item['archetype']

                    return response

        except Exception as error:
            print(error)
            raise HTTPException(status_code=400, detail="Bad request")

    def get_field_data(self, port):
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    result = db.execute(
                        """
                        SELECT archetype, position, owner, status, visibility, health
                        FROM field_tiles
                        WHERE game_port = %s;
                        """,
                        [port]
                    )
                    record = result.fetchall()
                    response = {}
                    for item in record:
                        response[item['position']] = {k: v for (k, v) in item.items() if k != "position"}

                    return response

        except Exception as error:
            print(error)
            raise HTTPException(status_code=400, detail="Bad request")

    def get_shade_data(self, port):
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    result = db.execute(
                        """
                        SELECT position, visibility, owner, turnWhenHidden, color
                        FROM shade_tiles
                        WHERE game_port = %s;
                        """,
                        [port]
                    )

                    record = result.fetchall()
                    response = {}
                    for item in record:
                        response[item['position']] = {k: v for (k, v) in item.items() if k != "position"}

                    return response

        except Exception as error:
            print(error)
            raise HTTPException(status_code=400, detail="Bad request")

    def get_game_data(self, port) -> GameDataOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                            total_turns,
                            current_turn,
                            player_count,
                            host,
                            player_1,
                            player_2,
                            player_3,
                            player_4,
                            player_1_int,
                            player_2_int,
                            player_3_int,
                            player_4_int

                        FROM games
                        WHERE port = %s;
                        """,
                        [port]
                    )

                    record = result.fetchone()

                    return GameDataOut(
                        total_turns=record[0],
                        current_turn=record[1],
                        player_count=record[2],
                        host=record[3],
                        player_1=record[4],
                        player_2=record[5],
                        player_3=record[6],
                        player_4=record[7],
                        player_1_int=record[8],
                        player_2_int=record[9],
                        player_3_int=record[10],
                        player_4_int=record[11]
                    )

        except Exception as error:
            print(error)
            raise HTTPException(status_code=400, detail="Bad request")

    def get_map_data(self, port) -> GameMapOut:

        if not value_in_column(value=port, table="games", col="port"):
            raise HTTPException(404, detail=f"Game not found at port: {port}")

        floor_tiles = self.get_floor_data(port=port)
        field_tiles = self.get_field_data(port=port)
        shade_tiles = self.get_shade_data(port=port)
        try:
            return GameMapOut(
                floor_tiles=floor_tiles,
                field_tiles=field_tiles,
                shade_tiles=shade_tiles
            )
        except Exception as error:
            print(error)
            raise HTTPException(status_code=400, detail="Bad request")

    def update_field_data(self, port, data):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM field_tiles
                        WHERE game_port = %s;
                        """,
                        [port]
                    )
                    print("fired")
                    updated_field = [
                        [
                            port,
                            data[key]["archetype"],
                            key, data[key]["owner"],
                            data[key]["status"],
                            json.dumps(data[key]["visibility"]),
                            data[key]["health"],
                        ]
                        for key in data
                        ]

                    for item in updated_field:
                        db.execute(
                            """
                            INSERT INTO field_tiles (game_port, archetype, position, owner, status, visibility, health)
                            VALUES (%s, %s, %s, %s, %s, %s, %s);
                            """,
                            item
                        )
                    return True

        except Exception as error:
            print(error)
            return error

    def update_shade_data(self, port, data):
        pass

    def update_meta_data(self, data):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE games
                        SET
                            total_turns = %s,
                            current_turn = %s,
                            player_1_int = %s,
                            player_2_int = %s,
                            player_3_int = %s,
                            player_4_int = %s

                        WHERE port = %s;
                        """,
                        [
                            data.total_turns,
                            data.current_turn,
                            data.player_1_int,
                            data.player_2_int,
                            data.player_3_int,
                            data.player_4_int,
                            data.port
                        ]

                    )
                    return True

        except Exception as error:
            print(error)
            return error

    def save_game(self, data: GameDataIn) -> Union[SaveSuccess, SaveFailure]:
        try:
            if data.field_tiles:
                self.update_field_data(port=data.port, data=data.field_tiles)

            self.update_meta_data(data=data)

            return SaveSuccess(
                message=f"Game at port: {data.port} saved!",
                status=200
            )

        except Exception as error:
            return SaveSuccess(
                message=f"Game at port: {data.port} failed to save! \n Error: {error}",
                status=401
            )

    def forfeit_game(self):
        pass

    def exit_game(self, port: str):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE games
                        SET is_active = FALSE
                        WHERE port = %s
                        """,
                        [port]
                    )
                    return True
        except Exception as error:
            logger.error("Error fetching active games: %s", error)
            return False

    def get_active_games(self) -> ActiveGamesOut:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    result = db.execute(
                        """
                        SELECT port, total_turns, player_count, player_1, player_2, player_3, player_4
                        FROM games
                        WHERE is_active = TRUE;
                        """
                    )

                    records = result.fetchall()

                    active_games = []
                    for record in records:
                        port = record["port"]
                        active_games.append(
                            ActiveGames(
                                port=port,
                                player_count=record["player_count"],
                                total_turns=record["total_turns"],
                                players=[record["player_1"], record["player_2"], record["player_3"], record["player_4"]]
                                ))

                    return ActiveGamesOut(active_games=active_games)
        except Exception as error:
            logger.error("Error fetching active games: %s", error)
            raise HTTPException(status_code=400, detail="Bad request")

    def get_users_games(self, username: str) -> ActiveGamesOut:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    result = db.execute(
                        """
                        SELECT port, total_turns, player_count, player_1, player_2, player_3, player_4
                        FROM games
                        WHERE total_turns > 0 AND
                            (%s = player_1 OR %s = player_2 OR %s = player_3 OR %s = player_4);
                        """,
                        [username, username, username, username]
                    )

                    records = result.fetchall()

                    active_games = []
                    for record in records:
                        port = record["port"]
                        active_games.append(
                            ActiveGames(
                                port=port,
                                player_count=record["player_count"],
                                total_turns=record["total_turns"],
                                players=[record["player_1"], record["player_2"], record["player_3"], record["player_4"]]
                            )
                        )

                    return ActiveGamesOut(active_games=active_games)
        except Exception as error:
            logger.error("Error fetching active games: %s", error)
            raise HTTPException(status_code=400, detail="Bad request")
