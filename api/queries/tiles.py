from pydantic import BaseModel
from queries.pool import pool
from psycopg.rows import dict_row
from fastapi.exceptions import HTTPException


class FieldArchetype(BaseModel):
    name: str
    type: str
    cost: int
    max_health: int
    sprite: dict
    detection_range: int
    tile_shape: str


class AllArchetypes(BaseModel):
    data: dict


class Movement(BaseModel):
    data: dict


class Abilities(BaseModel):
    unit_name: str
    ability_one: str
    ability_one_range: int
    ability_one_cost: int
    ability_two: str
    ability_two_range: int
    ability_two_cost: int


class AllAbilities(BaseModel):
    data: dict


class Archetype_Repo:
    """
    Methods:
        .get_all_field_archetypes(): gets all archetypes in the repository
        .get_field_archetype(): gets specific archetype by field_id
    """
    def get_all_field_archetypes(self) -> AllArchetypes:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM field_archetypes;
                        """
                    )
                    record = result.fetchall()
                    if record is None:
                        raise HTTPException(
                            status_code=404,
                            detail="Archetypes not generated"
                        )

                    response = {}
                    for item in record:
                        response[item['field_id']] = {k: v for k, v in item.items() if k != 'field_id'}
                    return AllArchetypes(data=response)

        except Exception as error:
            print(error)
            raise HTTPException(status_code=400, detail="Bad request")

    def get_field_archetype(self, field_id: int) -> FieldArchetype:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM field_archetypes
                        WHERE field_id = %s;
                        """,
                        [field_id]
                    )
                    record = result.fetchone()
                    if record is None:
                        raise HTTPException(
                            status_code=404,
                            detail="Archetype not found"
                        )

                    return FieldArchetype(
                        name=record['name'],
                        type=record['type'],
                        cost=record['cost'],
                        max_health=record['max_health'],
                        sprite=record['sprite'],
                        detection_range=record['detection_range'],
                        tile_shape=record['tile_shape'],
                    )

        except Exception as error:
            print(error)
            raise HTTPException(status_code=400, detail="Bad request")

    def get_moves(self) -> Movement:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM movement;
                        """
                    )
                    record = result.fetchall()
                    if record is None:
                        raise HTTPException(
                            status_code=404,
                            detail="No moves generated"
                        )
                    print(record)
                    response = {}
                    for item in record:
                        response[item['movement_id']] = {k: v for k, v in item.items() if k != 'movement_id'}
                    return Movement(data=response)

        except Exception as error:
            print(error)
            raise HTTPException(status_code=400, detail="Bad request")


class AbilitiesRepo:
    """
    methods:
    .getAllAbilities(): return a dictionary of all abilities in the repository
    """
    def getAllAbilities(self) -> AllAbilities:
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=dict_row) as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM abilities;
                        """
                    )
                    record = result.fetchall()
                    if record is None:
                        raise HTTPException(
                            status_code=404,
                            detail="Abilities not generated"
                        )
                    print(record)
                    response = {}
                    for item in record:
                        response[item.get('ability_id')] = {k: v for k, v in item.items() if k != 'ability_id'}
                    return AllAbilities(data=response)

        except Exception as error:
            print(error)
            raise HTTPException(status_code=400, detail="Bad request")
