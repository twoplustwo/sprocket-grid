from pydantic import BaseModel
from queries.pool import pool


class DuplicateAccountError(ValueError):
    pass


class AccountIn(BaseModel):
    first_name: str
    last_name: str
    email: str
    username: str
    password: str


class AccountOut(BaseModel):
    user_id: str
    email: str
    username: str


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountQueries:
    def record_to_account_out(self, record) -> AccountOutWithPassword:
        account_dict = {
            "user_id": record[0],
            "first_name": record[1],
            "last_name": record[2],
            "email": record[3],
            "username": record[4],
            "hashed_password": record[5]
        }
        return account_dict

    def create_account(self, user: AccountIn,
                       hashed_password: str) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO users
                            (first_name,
                            last_name,
                            email,
                            username,
                            hashed_password)
                        VALUES
                            (%s, %s, %s, %s, %s)
                        RETURNING
                        user_id,
                        first_name,
                        last_name,
                        email,
                        username,
                        hashed_password;
                        """,
                        [
                            user.first_name,
                            user.last_name,
                            user.email,
                            user.username,
                            hashed_password,
                        ]
                    )
                    user_id = result.fetchone()[0]
                    return AccountOut(
                        user_id=user_id,
                        email=user.email,
                        username=user.username,
                    )

        except Exception:
            return {"message": "Could not create a user"}

    def get(self, username: str) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        user_id,
                        first_name,
                        last_name,
                        email,
                        username,
                        hashed_password
                        FROM users
                        WHERE username = %s
                        """,
                        [username],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_account_out(record)
        except Exception:
            return {"message": "Could not get account"}

    def get_account(self, username: str) -> AccountOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        user_id,
                        email,
                        username
                        FROM users
                        WHERE username = %s;
                        """,
                        [username]
                    )
                    result = db.fetchone()
                    return AccountOut(
                        user_id=result[0],
                        email=result[1],
                        username=result[2]
                    )
        except Exception:
            return {"message": "Could not get account"}

    def get_all_accounts(self) -> list[AccountOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT
                    user_id,
                    email,
                    username
                    FROM users;
                    """
                )
                results = db.fetchall()
                return [AccountOut(
                    user_id=result[0],
                    email=result[1],
                    username=result[2]
                ) for result in results]
