from queries.tiles import Archetype_Repo, AllArchetypes, FieldArchetype, AbilitiesRepo, AllAbilities, Movement
from fastapi import APIRouter, Depends, Response, HTTPException
from authenticator import authenticator

router = APIRouter()


@router.get("/field_archetype", response_model=AllArchetypes)
def all_field_archetype(
    account: dict = Depends(authenticator.get_current_account_data),
    repo: Archetype_Repo = Depends(),
):
    if account:
        getAll = repo.get_all_field_archetypes()
        return getAll
    raise HTTPException(status_code=401, detail="Login Required")


@router.get("/field_archetype/{field_id}", response_model=FieldArchetype)
def get_field_archetype(
    field_id: str,
    account: dict = Depends(authenticator.get_current_account_data),
    repo: Archetype_Repo = Depends(),
):
    if account:
        getArch = repo.get_field_archetype(field_id)
        return getArch
    raise HTTPException(status_code=401, detail="Login Required")


@router.get("/abilities", response_model=AllAbilities)
def getAllAbilities(
    repo: AbilitiesRepo = Depends(),
    account: dict = Depends(authenticator.get_current_account_data),
):
    if account:
        getAll = repo.getAllAbilities()
        return getAll
    raise HTTPException(status_code=401, detail="Login Required")


@router.get("/movement", response_model=Movement)
def get_moves(
    response: Response,
    account: dict = Depends(authenticator.get_current_account_data),
    repo: Archetype_Repo = Depends(),
):
    if account:
        getAll = repo.get_moves()
        return getAll
    raise HTTPException(status_code=401, detail="Login Required")
