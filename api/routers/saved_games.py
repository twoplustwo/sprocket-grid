from fastapi import APIRouter, Depends, Response, HTTPException
from typing import Union
from authenticator import authenticator
from queries.saved_games import (
    Player,
    GameOut,
    GameOutError,
    JoinGameError,
    GameDataOut,
    GameDataIn,
    SaveSuccess,
    SaveFailure,
    GameMapOut,
    GameRepo,
    ActiveGamesOut
)

router = APIRouter()


@router.post("/api/game/create", response_model=Union[GameOut, GameOutError])
def create_game(
    response: Response,
    account: dict = Depends(authenticator.get_current_account_data),
    repo: GameRepo = Depends(),
):
    if account:
        player = Player(username=account.get("username"))
        result = repo.create(player)
        result.username = account.get("username")
        response.status_code = 200

        return result

    raise HTTPException(status_code=401, detail="Login Required")


@router.put("/api/game/join", response_model=Union[GameOut, JoinGameError])
def join_game(
    player: Player,
    response: Response,
    account: dict = Depends(authenticator.get_current_account_data),
    repo: GameRepo = Depends(),
):
    if account:
        player.username = account.get("username")
        result = repo.join_game(player)
        result.username = account.get("username")
        response.status_code = 200

        return result

    raise HTTPException(status_code=401, detail="Login Required")


@router.get("/api/game/get/{port}", response_model=GameDataOut)
def get_game_data(
    port: str,
    account: dict = Depends(authenticator.get_current_account_data),
    repo: GameRepo = Depends(),
):
    if account:
        result = repo.get_game_data(port)
        return result

    raise HTTPException(status_code=401, detail="Login Required")


@router.put("/api/game/update", response_model=Union[SaveSuccess, SaveFailure])
def save_game(
    data: GameDataIn,
    response: Response,
    account: dict = Depends(authenticator.get_current_account_data),
    repo: GameRepo = Depends(),
):
    if account:
        result = repo.save_game(data)
        response.status_code = result.status
        return result

    raise HTTPException(status_code=401, detail="Login Required")


@router.get("/api/game/get/map/{port}", response_model=GameMapOut)
def get_map_data(
    port: str,
    response: Response,
    account: dict = Depends(authenticator.get_current_account_data),
    repo: GameRepo = Depends(),
):
    if account:
        result = repo.get_map_data(port=port)
        response.status_code = 200
        return result

    raise HTTPException(status_code=401, detail="Login Required")


@router.get("/api/games", response_model=ActiveGamesOut)
def get_active_games(
    account: dict = Depends(authenticator.get_current_account_data),
    repo: GameRepo = Depends(),
):
    if account:
        getAll = repo.get_active_games()
        return getAll

    raise HTTPException(status_code=401, detail="Login Required")


@router.get("/api/game/exit/{port}", response_model=SaveSuccess)
def exit_game(
    port: str,
    response: Response,
    account: dict = Depends(authenticator.get_current_account_data),
    repo: GameRepo = Depends(),
):
    success = repo.exit_game(port)
    if success:
        response.status_code = 200
        return SaveSuccess(message=f"Game at port: {port} has been exited!", status=200)
    else:
        response.status_code = 404
        return SaveSuccess(message=f"Game at port: {port} does not exist!", status=404)


@router.get("/api/games/user", response_model=ActiveGamesOut)
def get_users_games(
    account: dict = Depends(authenticator.get_current_account_data),
    repo: GameRepo = Depends(),
):
    if account:
        users_games = repo.get_users_games(account.get("username"))
        return users_games

    raise HTTPException(status_code=401, detail="Unauthorized")
