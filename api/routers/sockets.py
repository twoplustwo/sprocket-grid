import socketio


sio = socketio.AsyncServer(
    async_mode="asgi",
    cors_allowed_origins=[],
    logger=True,
    # engineio_logger=True
)


app = socketio.ASGIApp(
    socketio_server=sio
)


@sio.event
async def connect(sid, environ, auth):
    return True


@sio.on("join_game")
async def join_game(sid, port):
    sio.enter_room(sid=sid, room=port, namespace=None)
    return True


@sio.on("lobby_updated")
async def lobby_updated(sid, data):
    await sio.emit("player_loaded", data=data, room=data.get("port"), skip_sid=sid)


@sio.on("disconnect_player")
async def player_disconnected(sid, data):
    await sio.emit("player_disconnected", data=data.get("data"), room=data.get("port"))


@sio.on("start_game")
async def start_game(sid, data):
    await sio.emit("game_redirect", data=data.get("data"), room=data.get("port"))


@sio.on("init_map_request")
async def init_map_received(sid, data):
    await sio.emit("init_map_received", room=data.get("port"), skip_sid=sid)


@sio.on("init_map")
async def init_map(sid, data):
    await sio.emit("init_map_response", data=data.get("data"), room=data.get("port"), skip_sid=sid)


@sio.on("local_map_update")
async def map_update(sid, data):
    await sio.emit("map_update", data=data.get("data"), room=data.get("port"), skip_sid=sid)


@sio.on("field_update_out")
async def field_update(sid, data):
    await sio.emit("field_update_in", data=data.get("data"), room=data.get("port"), skip_sid=sid)


@sio.on("next_turn_out")
async def turn_progression(sid, data):
    await sio.emit("next_turn_in", room=data.get("port"))


@sio.on("GAMEOVER")
async def get_end_message(sid, data):
    await sio.emit("end_message", room=data.get("port"), data=data.get("data"), skip_sid=sid)
