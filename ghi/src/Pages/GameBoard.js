import { useEffect, useState, useRef } from "react";
import axios from "axios";
import useToken from "@galvanize-inc/jwtdown-for-react";

import { socket, roomConnect } from "../Helpers/socket";
import { assignMapData } from "../Helpers/promises";

import CombinedPlayerFunction from "../Components/CombinedPlayerFunction";
import MapTemplate from "../Components/MapTemplate";



export default function GameBoard({gameInfo, setGameInfo, playerData, setPlayerData, unitData, abilityData, movementData}) {
    const [floorTiles, setFloorTiles] = useState()
    const [fieldTiles, setFieldTiles] = useState()
    const [currentTile, setCurrentTile] = useState()
    const [shadingTiles, setShadingTiles] = useState()
    const [menuSize, setMenuSize] = useState({height: "1rem", width: "1rem"})
    const [winConditionMet, setWinConditionMet] = useState()

    const [mapSize, setMapSize] = useState(270)

    const dataRef = useRef()
    dataRef.current = {floor_tiles: floorTiles, field_tiles: fieldTiles, shade_tiles: shadingTiles}

    const mapRef = useRef()
    const menuRef = useRef()

    const { token } = useToken()
    const config = {headers: {Authorization: `Bearer ${token}`}}

    const send_map = () => {
        if (playerData.username === gameInfo.host) {
            socket.emit("init_map", {port: playerData.port, data: dataRef.current})
        }
    }

    useEffect(() => {
        //
        socket.connect()

        const init_map = (response) => {
            assignMapData(response, setFloorTiles, setFieldTiles, setShadingTiles)
        }

        const syncField = (response) => {
            setFieldTiles((current) => {
                if (JSON.stringify(response) !== JSON.stringify(current)) {
                    return response
                } else {
                    return current
                }
            })
        }

        const turn_progression = () => {
            setGameInfo((current) => {
                let updatedInfo = {...current}
                const nextTurn = updatedInfo.current_turn === updatedInfo.player_count ? 1 : updatedInfo.current_turn + 1
                updatedInfo.current_turn = nextTurn
                updatedInfo.total_turns += 1
                return updatedInfo
            })
        }

        const request_map = () => {
            if (playerData.username !== gameInfo.host) {
                socket.emit("init_map_request", {port: playerData.port})
                setTimeout(() => {
                    if (!dataRef.current.floor_tiles || !dataRef.current.shade_tiles){
                        request_map()
                    }
                }, 1000)
            }
            else {
                if (playerData.username === gameInfo.host) {
                    axios.get(`${process.env.REACT_APP_USER_SERVICE_API_HOST}/api/game/get/map/${playerData.port}`, config)
                    .then((response) => {
                        assignMapData(response.data, setFloorTiles, setFieldTiles, setShadingTiles)
                        .then((response) => {
                            socket.emit("init_map", {"data": response, "port": playerData.port})
                        })
                    })
                    .catch((error) => {
                        console.log(error)
                    })
                }
            }
        }

        const game_end = (data) => {
            if (!winConditionMet) {
             setWinConditionMet(data)
            }
        }


        socket.on("connect", () => roomConnect(playerData.port, request_map))
        socket.on("init_map_received", send_map)
        socket.on("init_map_response", init_map)
        socket.on("field_update_in", syncField)
        socket.on("next_turn_in", turn_progression)
        socket.on("end_message", game_end)

        return () => {
            socket.off("init_map_received", send_map)
            socket.off("init_map_response", init_map)
            socket.off("field_update_in", syncField)
            socket.off("next_turn_in", turn_progression)
            socket.off("end_message", game_end)
            socket.disconnect()
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    useEffect(() => {
        const element = mapRef.current
        if (!element) {
            return
        }
        const observer = new ResizeObserver(() => {
            const {width, height} = element.getBoundingClientRect()
            let widthOrHeight = width > height ? height : width
            const adjustedSize = ((widthOrHeight * .8) - ((widthOrHeight * .8) % 18))

            if (adjustedSize > 0) {
                setMapSize(adjustedSize)
            }
        })

        observer.observe(element)

            return () => {
                observer.disconnect()
            }
    }, [])

    useEffect(() => {
        const element = menuRef.current

        if (!element) {
            return
        }

        const screenObserver = new ResizeObserver(() => {
            const {width, height} = element.getBoundingClientRect()
            setMenuSize({height: height, width: width})

        })

        screenObserver.observe(element)

        return () => {
            screenObserver.disconnect()
        }
    }, [])


    useEffect(() => {
        //
        if (fieldTiles) {
            socket.emit("field_update_out", {port: playerData.Port, data: fieldTiles})
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [fieldTiles])


    useEffect(() => {
        if (fieldTiles) {
            socket.emit("field_update_out", {port: playerData.Port, data: fieldTiles})
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [fieldTiles])

    const audioo = useRef()

    function playMusic(){
        audioo.current.volume = .25
        audioo.current.play()
    }

    useEffect(() => {
        playMusic()
    }, [])


    const onUnitClick = (key) => {
        setCurrentTile({...fieldTiles[key], position: key})
    }


    return(
        <div
            className="container-fluid d-flex flex-row align-items-center m-2"
            style={{
                    background: "linear-gradient(192deg, #181818 0%, #49494B 100%)",
                    borderRadius: "40px",
                    border: "1px solid #000",
                    boxShadow: "-3px 4px 4px 1px rgba(0, 0, 0, 0.25)",
                    height: "90%",
                    width: "95%",
                }}
            >
            <div className="container-fluid d-flex flex-column justify-content-center align-items-center w-60 h-100 m-4" ref={mapRef}>
                {
                    fieldTiles
                    ?
                    <MapTemplate
                        floor={floorTiles}
                        field={fieldTiles}
                        shade={shadingTiles}
                        responsiveSize={mapSize}
                        playerCount={gameInfo.player_count}
                        unitData={unitData}
                        playerData={playerData}
                        unitClick={onUnitClick}
                    />
                    :
                    <div className="container-fluid w-100 h-75">
                        <h1>LOADING...</h1>
                    </div>
                }
            </div>
            <audio ref={audioo} src="../assets/gamemusic.mp3" loop/>
            <div className="container-fluid d-flex align-items-center justify-content-center h-100 w-40 m-2 p-5 border-1">
                <CombinedPlayerFunction
                    playerData={playerData}
                    setPlayerData={setPlayerData}
                    gameInfo={gameInfo}
                    setGameInfo={setGameInfo}
                    fieldTiles={fieldTiles}
                    setFieldTiles={setFieldTiles}
                    setShadingTiles={setShadingTiles}
                    currentTile={currentTile}
                    unitData={unitData}
                    abilityData={abilityData}
                    movementData={movementData}
                    menuRef={menuRef}
                    menuSize={menuSize}
                    setCurrentTile={setCurrentTile}
                    winConditionMet={winConditionMet}
                    setWinConditionMet={setWinConditionMet}
                />
            </div>
        </div>
    )
}
