import { useState, useEffect, useRef } from "react";
import { useNavigate } from 'react-router-dom';
import axios from "axios";
import { assignPlayerData } from "../Helpers/promises";
import useToken from "@galvanize-inc/jwtdown-for-react";

import { BlkWarning, BlkGreen, BlkError } from "../Components/RetroControls/Blinkenlights";
import MainScreen from "../Components/HomeCrt/MainScreen";
import CreateGame from "../Components/HomeCrt/CreateGame";
import LoginError from "../Components/HomeCrt/LoginError"
import ServerList from "../Components/HomeCrt/ServerList"
import SwitchComps from "../Components/SwitchComps"

import "../crt.css"
import LoadSave from "../Components/HomeCrt/LoadSave";
import Stats from "../Components/HomeCrt/Stats";

export default function Home({setPlayerData, playerData}) {
    const navigate = useNavigate()

    const [port, setPort] = useState("")
    // const [errorMessage, setError] = useState()
    const [activeComp, setActiveComp] = useState("main")
    const { token } = useToken()

    const [screenSize, setScreenSize] = useState({height: "500px", width: "1000px"})
    const screenRef = useRef()

    useEffect(() => {

        const element = screenRef.current

        if (!element) {
            return
        }

        const screenObserver = new ResizeObserver(() => {
            const {width, height} = element.getBoundingClientRect()
            setScreenSize({height: height, width: width})

        })

        screenObserver.observe(element)

        return () => {
            screenObserver.disconnect()
        }
    }, [])


    const handleJoinSubmit = () => {
        const url = `${process.env.REACT_APP_USER_SERVICE_API_HOST}/api/game/join`
        const data = {port: port}
        const config = {headers: {Authorization: `Bearer ${token}`}}
        axios.put(url, data, config)
        .then((response) => {
            assignPlayerData(setPlayerData, response.data)
            .then(() => {
                navigate("/game/setup", {replace: true})
            })
        })
        .catch((error) => {
            // setError(error)
            console.log(error)
        })
    }


    const handleCreateSubmit = () => {
        axios.post(`${process.env.REACT_APP_USER_SERVICE_API_HOST}/api/game/create`, {}, {headers: {Authorization: `Bearer ${token}`}})
        .then((response) => {
            assignPlayerData(setPlayerData, response.data)
            .then(() => {
                navigate("/game/setup", {replace: true})
            })
        })
        .catch((error) => {
            // setError(error.response.data.message)
            console.log(error)
        })
    }



    return(
        <div className="container-fluid w-100 p-5" style={{height: "90%"}}>
            <div
                className="container-fluid d-flex flex-row justify-content-between p-5 m-0 h-100"
                style={{
                    background: "linear-gradient(192deg, #181818 0%, #49494B 100%)",
                    borderRadius: "40px",
                    border: "1px solid #000",
                    boxShadow: "-3px 4px 4px 1px rgba(0, 0, 0, 0.25)"
                }}>
                <div className="container-fluid d-flex flex-column justify-content-between align-items-center p-0 m-0 h-100 w-80">
                    <div className="container-fluid d-flex flex-row justify-content-between align-items-center p-0 m-0 h-75 w-100" ref={screenRef}>
                        <div className="pointScreen crt" style={{height: screenSize.height, width: screenSize.width}}>
                            <SwitchComps active={activeComp}>
                                <MainScreen name="main" />
                                <CreateGame
                                    name="create"
                                    handleCreateSubmit={handleCreateSubmit}
                                />
                                <LoadSave
                                    name="load"
                                    port={port}
                                    setPort={setPort}
                                    handleJoinSubmit={handleJoinSubmit}
                                    playerData={playerData}
                                    setPlayerData={setPlayerData}

                                />
                                <ServerList
                                    name="list"
                                    port={port}
                                    setPort={setPort}
                                    handleJoinSubmit={handleJoinSubmit}
                                />
                                <Stats name="stats" />
                                <LoginError />
                            </SwitchComps>
                        </div>
                    </div>
                    <div className="container-fluid d-flex flex-row justify-content-center align-items-center p-0 m-0 h-25 w-100">
                        <div className="container-fluid d-flex flex-row justify-content-center align-items-center p-0 m-0 h-100 w-80">
                            <div className="switch-panel container-fluid d-flex justify-content-center align-items-center h-75 m-0" >
                                <button className="mainframe-btn" onClick={() => setActiveComp("main")}>Main</button>
                                <button className="mainframe-btn" onClick={() => setActiveComp("create")}>Create</button>
                                <button className="mainframe-btn" onClick={() => setActiveComp("load")}>Load</button>
                                <button className="mainframe-btn" onClick={() => setActiveComp("list")}>List</button>
                                <button className="mainframe-btn" onClick={() => setActiveComp("stats")}>Stats</button>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="container-fluid d-flex flex-column justify-content-between align-items-center p-0 ms-4 h-100 w-20">
                    <div className="container-fluid d-flex flex-row p-4 m-0 w-100 h-75 lightPanel">
                        <div className="container-fluid d-flex flex-column align-items-center p-1 m-0 w-50 h-100">
                            <BlkGreen message="Main" turnOn={activeComp === "main" ? true : false}/>
                            <BlkGreen message="Create" turnOn={activeComp === "create" ? true : false}/>
                            <BlkGreen message="Load" turnOn={activeComp === "load" ? true : false}/>
                            <BlkGreen message="List" turnOn={activeComp === "list" ? true : false}/>
                            <BlkGreen message="Stats" turnOn={activeComp === "stats" ? true : false}/>
                        </div>
                        <div className="container-fluid d-flex flex-column align-items-center p-1 m-0 w-50 h-100">
                            <BlkError message={"Error"} turnOn={token ? false : true} />
                            <BlkWarning message="Login" turnOn={token ? false : true}/>
                        </div>
                    </div>
                    <div className="container-fluid d-flex flex-row justify-content-center align-items-center p-2 m-4 w-100 retro-dataplate">
                        <p className="p-0 m-0" style={{fontSize: ".5rem", textAlign: "center"}}>
                            <strong>ACHTUNG!</strong><br/>
                            ALLES TURISTEN UND NONTEKNISCHEN LOOKENSPEEPERS!
                            DAS KOMPUTERMASCHINE IST NICHT FÜR DER GEFINGERPOKEN UND MITTENGRABEN! ODERWISE IST EASY TO SCHNAPPEN DER SPRINGENWERK,
                            BLOWENFUSEN UND POPPENCORKEN MIT SPITZENSPARKEN.
                            IST NICHT FÜR GEWERKEN BEI DUMMKOPFEN. DER RUBBERNECKEN SIGHTSEEREN KEEPEN DAS COTTONPICKEN HÄNDER IN DAS POCKETS MUSS.
                            ZO RELAXEN UND WATSCHEN DER BLINKENLICHTEN.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}
