import { useEffect, useState, useRef } from "react";
import { useNavigate } from 'react-router-dom';
import Carousel from 'react-multi-carousel';
import useToken from "@galvanize-inc/jwtdown-for-react";

import axios from "axios";

import { socket, roomConnect } from "../Helpers/socket";
import { convertCords, ShadeTiles } from "../Helpers/MapFunctions";

import UnitCard from "../Components/LobbyCrt/UnitCard";
import DefaultUnitCard from "../Components/DefaultUnitCard";
import MapTemplate from "../Components/MapTemplate";
import NewGameCrt from "../Components/LobbyCrt/NewGame";
import LoadGameCrt from "../Components/LobbyCrt/LoadGameCrt";

import "./GameSetup.css"
import "../crt.css"
import 'react-multi-carousel/lib/styles.css';



export default function GameSetup({playerData, gameInfo, setGameInfo, unitData, setUnitData, setAbilityData, setMovementData}) {
    const { token } = useToken()
    const navigate = useNavigate()

    const [allReady, setAllReady] = useState(false)
    const [selectedUnit, setSelectedUnit] = useState()
    const [stagingFloor, setStagingFloor] = useState({})
    const [stagingField, setStagingField] = useState({})
    const [stageShade, setStageShade] = useState({})
    const [size, setSize] = useState()
    const [screenSize, setScreenSize] = useState({height: "1rem", width: "1rem"})
    const [isNew, setIsNew] = useState()
    const [players, setPlayers] = useState({
        1: {username: "", isReady: null},
        2: {username: "", isReady: null},
        3: {username: "", isReady: null},
        4: {username: "", isReady: null},
    })

    const gridRef = useRef()
    const screenRef = useRef()
    const playersRef = useRef()
    const fieldRef = useRef()

    playersRef.current = players
    fieldRef.current = stagingField

    const placedCommandCenter =
        Object.values(fieldRef.current).filter(
            (value) => value.owner === playerData.player_num && value.archetype === 8).length > 0
        ?
        true
        :
        false

    const shade = new ShadeTiles({setShadingTiles: setStageShade})
    const config = {headers: {Authorization: `Bearer ${token}`}}

    const playerColor = {
        1: "#34eb49",
        2: "#f54242",
        3: "#e8eb34",
        4: "#4287f5"
    }

    const responsive = {
        xl: {
            breakpoint: { max: 2000, min: 2500 },
            items: 6
        },
        lg: {
            breakpoint: { max: 2000, min: 1500 },
            items: 5
        },
        md: {
            breakpoint: { max: 1500, min: 1000 },
            items: 4,

        },
        sm: {
            breakpoint: { max: 1000, min: 500 },
            items: 3,
        },
        xsm: {
            breakpoint: { max: 500, min: 250 },
            items: 2,
        },
    };

    /**
     * Fires if the game is new and not loaded from a save,
     * generates then assigns the tiles necessary for a staging area.
     * differentiates each players start zone by color and only creates shading tiles for that start area.
     *
     * @returns {[null]} sets state for floor and shade tile variables
     */
    function generateStagingArea () {

        const selectPlayer = (col, row) => {
            if (col >= 3  && col <= 14 && row >= 0 && row <= 2) {
                const isCurrentUser = playerData.player_num === 1
                return players[1].username ? [isCurrentUser, "#34eb49"] : [false, "#c7c8c9"]
            }
            if (col >= 3  && col <= 14 && row >= 15 && row <= 17) {
                const isCurrentUser = playerData.player_num === 2
                return players[2].username ? [isCurrentUser, "#f54242"] : [false, "#c7c8c9"]
            }
            if (col >= 0  && col <= 2 && row >= 3 && row <= 14) {
                const isCurrentUser = playerData.player_num === 3
                return players[3].username ? [isCurrentUser, "#e8eb34"] : [false, "#c7c8c9"]
            }
            if (col >= 15  && col <= 17 && row >= 3 && row <= 14) {
                const isCurrentUser = playerData.player_num === 4
                return players[4].username ? [isCurrentUser, "#4287f5"] : [false, "#c7c8c9"]
            }
            return [false, "#c7c8c9"]
        }

        let floorStore = {}
        let shadeStore = {}
            for (let col = 0; col <= 17; col++){
                for (let row = 0; row <= 17; row++){

                    let cord = convertCords([col, row])
                    let playerCheck = selectPlayer(col, row)

                    floorStore[cord] = {"background": playerCheck[1]}

                    if (playerCheck[0]) {
                        shadeStore[cord] = {visibility: "hidden", onClick: null, color: "#c7c8c9"}
                    }
                }
            }
            setStagingFloor((current) => {
                let updatedStage = {...current, ...floorStore}
                return updatedStage
            })

            setStageShade(shadeStore)
        }

    /**
     * Initializes the player list with the current user and a "isReady" status of "Not Ready"
     *
     */
    function assignJoin() {
        setPlayers((current) => {
            let newLobby = {...current}
            let user = {username: null, isReady: null}
            user.username = playerData.username
            user.isReady = "Not Ready"
            newLobby[playerData.player_num] = user
            return newLobby
        })
    }


    function assignGameInfo(response) {
        return new Promise((resolve) => {
            setGameInfo(response, resolve(response.total_turns))
        })
    }

    function assignNewGame(total_turns) {
        return new Promise(resolve => {
            if (total_turns === 0) {
                setIsNew(true, resolve())
            } else {
                setIsNew(false, resolve())
            }
        })
    }


    /**
     * Toggles isReady status
     * @param {*} event onClick event
     */
    function readyUp(event) {
        event.preventDefault()
        const readyOptions = {
            "Not Ready": "Ready",
            "Ready": "Not Ready"
        }
        let prevStatus = players[playerData.player_num].isReady

        setPlayers((current) => {
            let updatedLobby = {...current}
            const result = readyOptions[prevStatus]
            updatedLobby[playerData.player_num].isReady = result

            return updatedLobby
        })
    }

    /**
     * If a game is 2 players, the coordinates of the staging map don't match the coordinates of a 2 player game map
     * shifts the x-cord of all units to the left by 3 tiles
     * fired on game start if the game is new and the player count < 3
     * @returns {[null]}
     */
    function translateCords() {
        let newObject = {}
        Object.keys(stagingField).forEach((oldKey) => {
            const posArray = convertCords(oldKey)
            posArray[0] -= 3
            const newKey = convertCords(posArray)
            newObject[newKey] = stagingField[oldKey]
        })
        return newObject
    }

    /**
     * Checks if the game is new and has 2 players, translates cords if true
     * updates the game entry in the backend with all data collected during game setup.
     *
     * gets an updated ledger of game meta data.
     *
     * emits a websocket message to all players in the same room with the updated meta data.
     *
     * websocket message triggers redirect to game page for all connected players.
     */
    function startGame() {
        const player_count = Object.keys(playersRef.current).filter(key => playersRef.current[key].username !== "").length
        let response = stagingField
        if (player_count <= 2 && isNew) {
            response = translateCords()
        }

        axios.put(`${process.env.REACT_APP_USER_SERVICE_API_HOST}/api/game/update`, {
                port: playerData.port,
                total_turns: 1,
                current_turn: 1,
                player_1_int: gameInfo.player_1_int,
                player_2_int: gameInfo.player_2_int,
                player_3_int: gameInfo.player_3_int,
                player_4_int: gameInfo.player_4_int,
                floor_tiles: {},
                field_tiles: response,
                shade_tiles: {}
            }, config
        )
        .then(() => {
            axios.get(`${process.env.REACT_APP_USER_SERVICE_API_HOST}/api/game/get/${playerData.port}`, config)
            .then((response) => {
                socket.emit("start_game", {port: playerData.port, data: response.data})
            })
        })
        .catch((error) => {
            console.error(error)
        })

        .catch((error) => {
            console.error(error)
        })
    }

    /**
     * sets the current unit to the clicked carousel card, triggers the unit detail menu to render info about the selected unit
     * @param {Number} unit_id id of unit archetype selected
     */
    function unitOnClick(unit_id) {
        setSelectedUnit({unit_id: unit_id, data: unitData[unit_id]})
    }

    /**
     *
     * @param {*} event
     */
    function clickShade(event) {
        const defaultHidden = () => {
                let visObj = {}
                for (let num = 1; num <= 4; num++){
                    if (num === playerData.player_num) {
                        visObj[num] = "visible"
                    } else {
                        visObj[num] = "hidden"
                    }
                }
                return visObj
            }


        const initiative = gameInfo[`player_${playerData.player_num}_int`]

        if (initiative > 0 && initiative - (placedCommandCenter ? selectedUnit.data.cost : unitData[8].cost) >= 0) {
            setStagingField((current) => {
                let updatedField = {...current}
                let newTile = {}
                newTile.archetype = placedCommandCenter ? selectedUnit.unit_id : 8
                newTile.owner = playerData.player_num
                newTile.status = "alive"
                newTile.visibility =
                    placedCommandCenter
                    ?
                        selectedUnit.unit_id === "2" || selectedUnit.unit_id === "5"
                        ?
                        defaultHidden()
                        :
                        {1: "visible", 2: "visible", 3: "visible", 4: "visible"}
                    :
                    {1: "visible", 2: "visible", 3: "visible", 4: "visible"}
                newTile.health = placedCommandCenter ? selectedUnit.data.max_health : unitData[8].max_health
                updatedField[event.target.id] = newTile

                return updatedField
            })

            shade.reset()

            setGameInfo((current) => {
                let updatedInt = {...current}
                updatedInt[`player_${playerData.player_num}_int`] -= placedCommandCenter ? selectedUnit.data.cost : unitData[8].cost
                return updatedInt
            })

        }
        else {
            shade.reset()
        }

    }

    function onSelect() {
        let cordParams = []
        Object.keys(stageShade).filter(key => !stagingField[key]).forEach((cord) => {
            let new_param = {}
            new_param.position = cord
            new_param.color = "#90EE90"
            new_param.isVisible = true
            new_param.onClick = clickShade
            new_param.turnWhenHidden = null
            cordParams.push(new_param)
        })

        shade.set(cordParams)
    }

    function commandCenterSelect() {

        let cordParams = []
        Object.keys(stageShade).filter(key => !stagingField[key]).forEach((cord) => {
            let new_param = {}
            new_param.position = cord
            new_param.color = "#90EE90"
            new_param.isVisible = true
            new_param.onClick = clickShade
            new_param.turnWhenHidden = null
            cordParams.push(new_param)
        })

        shade.set(cordParams)

    }

    function lobbyDisconnect() {
        const player_count = Object.keys(playersRef.current).filter(key => playersRef.current[key].username !== "").length
        if (player_count === 1 && gameInfo.total_turns === 0) {
            axios.get(`${process.env.REACT_APP_USER_SERVICE_API_HOST}/api/game/exit/${playerData.port}`, config)
        }
        else {
            socket.emit("disconnect_player", {port: playerData.port, data: playerData.player_num})
        }
    }

    function refundUnit(key) {
        const refundValue = unitData[fieldRef.current[key].archetype].cost
        setStagingField((current) => {
            let updatedField = {...current}
            delete updatedField[key]
            return updatedField
        })

        setGameInfo((current) => {
            let updatedInt = {...current}
            updatedInt[`player_${playerData.player_num}_int`] += refundValue
            return updatedInt
        })

        if (fieldRef.current[key].archetype === 8) {
            setPlayers((current) => {
                let updatedLobby = {...current}
                const result = "Not Ready"
                updatedLobby[playerData.player_num].isReady = result
                return updatedLobby
            })
        }
    }


    useEffect(() => {
        //
        const initialize = async () => {
            await Promise.all([
                await axios.get(`${process.env.REACT_APP_USER_SERVICE_API_HOST}/field_archetype`, config),
                await axios.get(`${process.env.REACT_APP_USER_SERVICE_API_HOST}/api/game/get/${playerData.port}`, config)
            ])
            .then(async (response) => {
                setUnitData(response[0].data.data)
                await assignGameInfo(response[1].data)
                .then(async (response) => {
                    await assignNewGame(response)
                })
            })
            .catch((error) => {
                console.error(error)
            })

        }

        axios.get(`${process.env.REACT_APP_USER_SERVICE_API_HOST}/abilities`, config)
        .then((response) => {
            setAbilityData(response.data.data)
            })
        .catch((error) => {
            console.log(error)
        })

        axios.get(`${process.env.REACT_APP_USER_SERVICE_API_HOST}/movement`, config)
        .then((response) => {
            console.log(response.data.data)
            setMovementData(response.data.data)
            })
        .catch((error) => {
            console.log(error)
        })


        function onPlayerUpdate(response) {
            const playerKeys = Object.keys(response.players).map(Number)
            const joinedPlayers = playerKeys.filter(key => response.players[key].username !== "")

            if (joinedPlayers.length <= 4 && joinedPlayers.length > 0) {
                let new_players = {}

                for (let key of joinedPlayers) {
                    new_players[key] = response.players[key]
                }

                setPlayers((current) => {
                    let updatedLobby = {...current, ...new_players}

                    if (JSON.stringify(updatedLobby) !== JSON.stringify(current)) {
                        return updatedLobby
                    }
                    else {
                        return current
                    }
                })

                setGameInfo((current) => {
                    let updatedGame = {...current}
                    updatedGame[`player_${response.playerNum}_int`] = response.initiative
                    return updatedGame
                })
            }
        }

        function onPlayerDisconnected(response) {
            setPlayers((current) => {
                let updatedLobby = {...current}
                let user = {username: "", isReady: null}
                updatedLobby[response] = user
                return updatedLobby
            })
            if (isNew) {
                setStagingField((current) => {
                    let updatedField = {}
                    Object.keys(current).filter(key => current[key].owner !== response).forEach((tile) => {
                        updatedField[tile] = current[tile]
                    })
                    return updatedField
                })
            }
        }

        function onMapUpdate(data) {
            setStagingField((current) => {
                if (JSON.stringify(current) !== JSON.stringify(data)){
                    return data
                } else {
                    return current
                }
            })
        }

        function onGameRedirect(response) {
            assignGameInfo(response, setGameInfo)
            .then(() => {
                navigate("/game/play")
            })
            .catch((error) => {
                console.log(error)
            })
        }


        socket.connect()

        socket.on("connect", () => roomConnect(playerData.port, initialize))
        socket.on("player_loaded", onPlayerUpdate)
        socket.on("player_disconnected", onPlayerDisconnected)
        socket.on("map_update", onMapUpdate)
        socket.on("game_redirect", onGameRedirect)

        return () => {
            lobbyDisconnect()
            socket.off("player_loaded", onPlayerUpdate)
            socket.off("player_disconnected", onPlayerDisconnected)
            socket.off("map_update", onMapUpdate)
            socket.off("game_redirect", onGameRedirect)
            socket.disconnect()
            setPlayers({
                1: {username: "", isReady: null},
                2: {username: "", isReady: null},
                3: {username: "", isReady: null},
                4: {username: "", isReady: null},
            })
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    useEffect(() => {
        //
        if (players[playerData.player_num].username !== ""){
            socket.emit("lobby_updated",
                {
                    port: playerData.port,
                    players: players,
                    playerNum: playerData.player_num,
                    initiative: gameInfo[`player_${playerData.player_num}_int`]
                }
            )
            const ready_list = Object.keys(players).map((key) => {
                return players[key].isReady
            })

            if (!ready_list.includes("Not Ready")){
                setAllReady(true)
            }
            else {
                setAllReady(false)
            }



            if (isNew) {
                generateStagingArea()
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [players])


    useEffect(() =>{
        //
        if (isNew === false && !Object.keys(stagingFloor).length > 0) {

            assignJoin()

            axios.get(`${process.env.REACT_APP_USER_SERVICE_API_HOST}/api/game/get/map/${playerData.port}`, config)
            .then((response) => {
                setStagingFloor(response.data.floor_tiles)
                setStagingField(response.data.field_tiles)
            })
        }

        if (isNew){

            assignJoin()

        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isNew])


    useEffect(() => {
        //
        if (stagingField) {
            socket.emit("local_map_update", {"port": playerData.port, "data": stagingField})
            console.log(stagingField)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [stagingField])


    useEffect(() => {
        const breakpoints = {270 :180, 360: 270, 450: 360, 540: 450, 630: 540}
        const element = gridRef.current

        if (!element) {
            return
        }

        const observer = new ResizeObserver(() => {
            const width = element.getBoundingClientRect().width
            const selection = Object.keys(breakpoints).reduce((curr, prev) => Math.abs(curr - width) < Math.abs(prev - width) ? curr : prev)
            setSize(breakpoints[selection])
        })


        observer.observe(element)

        return () => {
            observer.disconnect()
        }
    }, [])


    useEffect(() => {
        const element = screenRef.current

        if (!element) {
            return
        }

        const screenObserver = new ResizeObserver(() => {
            const {width, height} = element.getBoundingClientRect()
            setScreenSize({height: height, width: width})

        })

        screenObserver.observe(element)

        return () => {
            screenObserver.disconnect()
        }
    }, [])



    return(
        <div id="stagingDiv">
            <div
                id="purpleBox"
                className="container-fluid d-flex flex-row w-100"
                style={{
                    visibility: stagingFloor && stageShade && unitData ? "visible" : "hidden"
                }}
            >
                <div className="container-fluid d-flex flex-column align-items-center w-40" ref={gridRef}>
                    {
                        unitData
                        ?
                        <MapTemplate
                            floor={stagingFloor}
                            field={stagingField}
                            shade={stageShade}
                            responsiveSize={size}
                            playerCount={isNew ? 4 : gameInfo.player_count}
                            unitData={unitData}
                            playerData={playerData}
                            unitClick={isNew ? refundUnit : null}

                        />
                        :
                        null
                    }
                    <div className="container-fluid d-flex justify-content-center w-75 p-0 bg-accentGrey p-2 gap-1" id="lobbyTable" style={{width: size}}>
                        <table className="table table-striped table-dark table-sm" style={{margin: 0, border: "2px inset black"}}>
                            <tbody>
                            {
                                Object.keys(players).map((key) => {
                                    return(
                                        <tr key={key}>
                                            <th className="w-25" style={{color: playerColor[key]}} > Player {key}: </th>
                                            <th style={{color: playerColor[key]}} > {players[key].username} </th>
                                            <td style={{textAlign: "center", verticalAlign: "middle"}}>
                                                <div
                                                    style={{
                                                        backgroundImage:
                                                        players[key].isReady === "Ready"
                                                            ?
                                                            "radial-gradient(#01750a, #12b504, #6eff61)"
                                                            :
                                                            "linear-gradient(#454745, #454745)",

                                                        height: "1rem",
                                                        width:"1rem",
                                                        borderRadius: "50%",
                                                        border: "1px inset #000000",
                                                        display: "inline-block",
                                                    }}
                                                >
                                                </div>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="container-fluid d-flex flex-column justify-content-center align-items-center w-60 bg-accentGrey" id="stageMenu">

                    <div className="container-fluid d-flex flex-row h-50">
                        <div className="container-fluid w-100 h-100 p-2 m-2 ">

                            <div className="container-fluid d-flex flex-row justify-content-between align-items-center p-0 m-0 h-100" ref={screenRef}>
                                <div className="pointScreen crt" style={{width: screenSize.width, height: screenSize.height}}>
                                    {
                                        isNew
                                        ?
                                        <NewGameCrt
                                            playerData={playerData}
                                            gameInfo={gameInfo}
                                            readyUp={readyUp}
                                            allReady={allReady}
                                            startGame={startGame}
                                            selectedUnit={selectedUnit}
                                            onSelect={onSelect}
                                            unitData={unitData}
                                            placedCommandCenter={placedCommandCenter}
                                            commandCenterSelect={commandCenterSelect}
                                            />
                                        :
                                        <LoadGameCrt
                                            playerData={playerData}
                                            gameInfo={gameInfo}
                                            readyUp={readyUp}
                                            allReady={allReady}
                                            startGame={startGame}
                                        />
                                    }
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr className="bg-offWhite border-2 border-top w-85" />
                    <div
                        className="container-fluid h-50 w-100 m-2"
                        style={{
                            visibility: isNew === true && placedCommandCenter  ? "visible": "hidden"
                        }}>
                        <Carousel
                            className="h-100"
                            responsive={responsive}
                            ssr={true}
                            infinite={true}
                            centerMode={false}
                            containerClass="carousel-container"
                            sliderClass="h-100"
                            itemClass="d-flex justify-content-center p-2 h-100"
                            dotListClass="custom-dot-list-style"

                        >
                            {
                                unitData
                                ?
                                Object.entries(unitData).filter(([unit_id, archetype]) => archetype.type === "Unit").map(
                                    ([unit_id, archetype]) =>

                                        <UnitCard
                                            key={unit_id + "carousel"}
                                            playerData={playerData}
                                            unit_id={unit_id}
                                            archetype={archetype}
                                            onClick={unitOnClick}
                                        />
                                    )
                                :
                                <DefaultUnitCard />
                            }
                        </Carousel>
                    </div>
                </div>
            </div>
        </div>
    )
}
