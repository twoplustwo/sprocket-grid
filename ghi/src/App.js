import { useState } from "react";
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from "./Pages/Home.js";
import Nav from "./Components/Nav.js";
import GameSetup from "./Pages/GameSetup.js";
import GameBoard from "./Pages/GameBoard.js";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react"
import "./App.css"


function App() {
  const [playerData, setPlayerData] = useState({username: "", port: null, player_num: 0 })
  const [gameInfo, setGameInfo] = useState({})
  const [unitData, setUnitData] = useState()
  const [abilityData, setAbilityData] = useState()
  const [movementData, setMovementData] = useState()

  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, '');

  return(
    <AuthProvider baseUrl={process.env.REACT_APP_USER_SERVICE_API_HOST}>
      <BrowserRouter basename={basename}>
      <Nav setPlayerData={setPlayerData}/>
      <div className="container-fluid d-flex align-items-center justify-content-center p-0 m-0 primary-bg-style">
        <Routes>
          <Route path="/" element={ <Home playerData={playerData} setPlayerData={setPlayerData} /> } />

          <Route path="game">

            <Route path="setup" element={
              <GameSetup
                playerData={playerData}
                gameInfo={gameInfo}
                setGameInfo={setGameInfo}
                unitData={unitData}
                setUnitData={setUnitData}
                setAbilityData={setAbilityData}
                setMovementData={setMovementData}
              /> }
            />

            <Route path="play" element={
              <GameBoard
                gameInfo={gameInfo}
                setGameInfo={setGameInfo}
                playerData={playerData}
                setPlayerData={setPlayerData}
                unitData={unitData}
                abilityData={abilityData}
                movementData={movementData}
              /> }
            />

          </Route>
        </Routes>
        </div>
      </BrowserRouter>
    </AuthProvider>
  )
}

export default App;
