import GridLayout from "react-grid-layout"

export default function MapTemplate({floor, field, shade, responsiveSize, playerCount, unitData, playerData, unitClick}) {
    const columnNum = 12 + ((playerCount - 2) * 3)
    const adjWidth = (responsiveSize / 18) * columnNum

    return (
        <GridLayout
            id="gridDiv"
            className="layout mb-3"
            cols={columnNum}
            rowHeight={responsiveSize / 18}
            width={adjWidth}
            allowOverlap={true}
            margin={[0, 0]}
            isDroppable={false}
            isDraggable={false}
            style={{aspectRatio: columnNum / 18}}
        >

            {
                floor
                ?
                Object.keys(floor).map(
                    key =>
                    <div
                        key={"a"+key}
                        data-grid={{ x: Number(key.split(",")[0]), y: Number(key.split(",")[1]), w: 1, h: 1, static: "true" }}
                        style={{
                            backgroundColor:
                                floor[key].background
                                ?
                                floor[key].background
                                :
                                "#c7c8c9",
                            border: ".5px solid #28292e"
                        }}
                    >
                    </div>
                )
                :
                null
            }

            {
                field
                ?
                Object.keys(field).map(
                    key => {
                        let imageArray = unitData[field[key].archetype].sprite[field[key].owner]
                        let unitHealth = field[key].health

                        return(
                            <div
                                key={"b"+key}
                                data-grid={{ x: Number(key.split(",")[0]), y: Number(key.split(",")[1]), w: 1, h: 1}}
                                style={{
                                    visibility: field[key]["visibility"][playerData.player_num]
                                }}
                            >
                                <button
                                    style={{
                                        backgroundColor: 'transparent',
                                        border: 0, padding: 0
                                        }}
                                    onClick={playerData.player_num === field[key].owner ? () => unitClick(key) : null}
                                    disabled={playerData.player_num === field[key].owner ? false : true}
                                    height={responsiveSize / 18}
                                    width={adjWidth / columnNum}
                                >
                                    <img
                                        src={unitHealth > 0 ? imageArray[0] : imageArray[1]}
                                        alt="Unit"
                                        height={responsiveSize / 18}
                                        width={adjWidth / columnNum}
                                    />
                                </button>
                            </div>
                        )
                    }
                )
                :
                null
            }

            {
                shade
                ?
                Object.keys(shade).map(
                    key =>
                        <div
                            key={"c"+key}
                            id={key}
                            data-grid={{ x: Number(key.split(",")[0]), y: Number(key.split(",")[1]), w: 1, h: 1}}
                            style={{
                                backgroundColor: shade[key].color,
                                visibility: shade[key].visibility,
                                border: "1px solid black",
                                opacity: 0.5
                            }}
                            onClick={shade[key].onClick}
                        >
                        </div>
                )
                :
                null
            }

        </GridLayout>
    )
}