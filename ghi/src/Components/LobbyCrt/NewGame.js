import { Fragment } from "react"
import UnitDetails from "./UnitDetails";

export default function NewGameCrt({
    playerData,
    gameInfo,
    readyUp,
    allReady,
    startGame,
    selectedUnit,
    onSelect,
    unitData,
    placedCommandCenter,
    commandCenterSelect
}) {
    return(
        <Fragment>
            <div className="container-fluid w-20 h-100 p-4">
                <h4 className="text-screenTextGreen">Port: {playerData.port}</h4>
                <h4 className="text-screenTextGreen">Points: {gameInfo[`player_${playerData.player_num}_int`]}</h4>
                {
                    placedCommandCenter
                    ?
                    <button className="crt-btn" onClick={(event) => {readyUp(event)}} type="button" >Ready Up</button>
                    :
                    null
                }
                {
                    playerData.username === gameInfo.host
                    ?
                    allReady
                        ?
                        <button className="crt-btn" onClick={startGame}>Start Game</button>
                        :
                        null
                    :
                    null
                }
            </div>
            <div className="vr border border-screenTextGreen border-1 crt-border opacity-75 m-4"></div>
            {
                placedCommandCenter
                ?
                selectedUnit && selectedUnit.unit_id !== 8
                    ?
                    <UnitDetails
                        playerData={playerData}
                        archetype={selectedUnit}
                        onSelect={onSelect}
                    />
                    :
                    null
                :
                <UnitDetails
                    playerData={playerData}
                    archetype={{unit_id: 8, data: unitData[8]}}
                    onSelect={commandCenterSelect}
                />
            }
        </Fragment>
    )
}