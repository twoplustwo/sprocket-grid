import "./UnitCard.css"


export default function UnitCard ({playerData, unit_id, archetype, onClick}) {
    const sprite = archetype.sprite[playerData.player_num][0]
    return (
        <div className="card h-100 bg-offWhite" id="unit-card" onClick={() => onClick(unit_id)}>
            <div className="card-header p-1">
                <p className="card-title text-center h5">{archetype.name}</p>
            </div>
            <img className="card-img-top p-2" src={sprite} alt="unit-card" />
        </div>
    )
}
