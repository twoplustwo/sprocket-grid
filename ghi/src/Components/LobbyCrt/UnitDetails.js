
export default function UnitDetails({playerData, archetype, onSelect}) {
    return (
        <div
            className="container h-100 p-4 w-80 crt-div"
        >
            <h4 className="text-decoration-underline">{archetype.data.name}</h4>
            <h5>Cost: {archetype.data.cost}</h5>
            <h5>Detection Range: {archetype.data.detection_range}</h5>
            {
                playerData.initiative > 0 && playerData.initiative - archetype.data.cost >= 0
                ?
                null
                :
                <h5>Error: Insufficient Points</h5>
            }

            <button
                disabled={playerData.initiative > 0 && playerData.initiative - archetype.data.cost >= 0 ? null : "disabled"}
                className="crt-btn"
                onClick={onSelect}
            >
                Purchase
            </button>
        </div>
    )
}