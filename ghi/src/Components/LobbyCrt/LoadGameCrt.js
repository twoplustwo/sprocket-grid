import { Fragment } from "react";

export default function LoadGameCrt({gameInfo, playerData, readyUp, allReady, startGame}) {
    return (
        <Fragment>
             <div className="container-fluid w-100 h-100 p-4">
                <h4 className="text-screenTextGreen">Port: {playerData.port}</h4>
                <h4 className="text-screenTextGreen">Current Initiative: {gameInfo[`player_${playerData.player_num}_int`]}</h4>
                <h4 className="text-screenTextGreen">Turns Elapsed: {gameInfo.total_turns}</h4>
                <h4 className="text-screenTextGreen">Currently Player {gameInfo.current_turn}'s Turn</h4>
                <button className="crt-btn" onClick={(event) => {readyUp(event)}} type="button" >Ready Up</button>
                {
                    allReady
                    ?
                    <button className="crt-btn" onClick={startGame}>Start Game</button>
                    :
                    null
                }
            </div>
        </Fragment>
    )
}