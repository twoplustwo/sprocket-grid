

export default function SwitchComps({ children, active}) {
    return children.filter(child => child.props.name === active)
}
