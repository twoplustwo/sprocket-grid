
export default function MenuDetails({ unitData, currentTile, abilityData, attackMenu, moveMenu, movementData }) {
  if (!currentTile || !unitData[currentTile.archetype] || !abilityData[currentTile.archetype]) {
    return <div>Select an Action</div>;
  }

  const { archetype } = currentTile;
  const unitName = unitData[archetype].name;
  const abilityOne = abilityData[archetype].ability_one;
  const abilityOneCost = abilityData[archetype].ability_one_cost;
  const abilityOneRange = abilityData[archetype].ability_one_range;
  const abilityTwo = abilityData[archetype].ability_two;
  const abilityTwoCost = abilityData[archetype].ability_two_cost;
  const abilityTwoRange = abilityData[archetype].ability_two_range;

  const marchRange = movementData[archetype].march
  const marchCost = movementData[archetype].march_cost
  const sneakRange = movementData[archetype].sneak
  const sneakCost = movementData[archetype].sneak_cost
  const travelRange = movementData[archetype].travel
  const travelCost = movementData[archetype].travel_cost

  return (
    <div className="container-fluid d-flex align-items-center p-0 m-0 h-100 w-35">
      <h3>{unitName} Stats</h3>
      { attackMenu && (
      <>
      <ul className="m-3 p-2 w-35 container-fluid">
        <li className="fs-4">{abilityOne}: {abilityOneCost} Initiative</li>
        <li className="fs-4">Range: {abilityOneRange}</li>
        {abilityTwo && (
          <>
            <li className="fs-4">{abilityTwo}: {abilityTwoCost} initiative</li>
            <li className="fs-4">Range: {abilityTwoRange}</li>
          </>
        )}
      </ul>
      </>
      )}
      { moveMenu && (
      <>
      <ul className="m-3 p-2 w-35 container-fluid">
        <li className="fs-4">March: {marchCost} Initiative</li>
        <li className="fs-4">Range: {marchRange}</li>
        {sneakRange > 0 && (
          <>
            <li className="fs-4">Sneak: {sneakCost} Initiative</li>
            <li className="fs-4">Range: {sneakRange}</li>
          </>
        )}
        {travelRange > 0 && (
          <>
            <li className="fs-4">Travel: {travelCost} Initiative</li>
            <li className="fs-4">Range: {travelRange}</li>
          </>
        )}
      </ul>
      </>
      )}
      <div className="w-50 container-fluid d-flex align-items-center"></div>

      <div className="p-2 container-fluid"></div>
      <div className="vr border border-screenTextGreen border-1 crt-border opacity-75 m-4 pr-1"></div>
    </div>
  );
}
