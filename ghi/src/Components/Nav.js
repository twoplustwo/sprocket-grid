
import { NavLink, useNavigate } from 'react-router-dom';
import useToken from '@galvanize-inc/jwtdown-for-react';
import LoginModal from './LoginModal';
import CreateAccountModal from './CreateAccountModal';
import '../App.css';
import { Fragment } from 'react';

export default function Nav( { setPlayerData } ) {

    const { logout, login, token } = useToken();
    const navigate = useNavigate()


    const handleClick = (e) => {
        e.preventDefault()
        logout()
        navigate("/")
    }

    return(
        <nav
            className="navbar navbar-expand nav-style">
            <div className="Container-fuild">
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0 p-2 ">
                        <h1 className="nav-text navbar-brand">Sprocket Grid</h1>
                        {
                        token
                        ?
                        <Fragment>
                            <NavLink className="nav-link nav-text" to="/">Play</NavLink>

                            <button id="logoutbtn" type="button" className="btn nav-text" onClick={handleClick}>Logout</button>
                        </Fragment>
                        :
                        <Fragment>
                            <LoginModal login={login} navigate={navigate} setPlayerData={setPlayerData}/>
                            <CreateAccountModal login={login} navigate={navigate} setPlayerData={setPlayerData}/>
                        </Fragment>
                        }
                    </ul>
                </div>
            </div>
        </nav>
    )
}
