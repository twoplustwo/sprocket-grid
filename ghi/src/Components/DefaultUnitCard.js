export default function DefaultUnitCard() {
    return(
        <div className="container d-flex bg-offWhite">
            <img src="https://www.nicepng.com/png/detail/933-9332131_profile-picture-default-png.png" alt="unit-card" />
            <h2>Name</h2>
            <hr className="bg-accentGrey border-2 border-top"/>
            <p>Cost</p>
        </div>
    )
}
