import React, { useState, useEffect, Fragment } from 'react';
import { validTiles } from '../Helpers/ValidTile';
import { moveTo } from '../Helpers/moveTo';
import MenuDetails from './MenuDetails';
import { socket } from "../Helpers/socket";
import { attackTarget } from '../Helpers/Attack';
import { ShadeTiles } from '../Helpers/MapFunctions';
import { WinCondition } from '../Helpers/WinCondition';
import WinModal from './WinModal'


function CombinedPlayerFunction({
  playerData, setPlayerData, gameInfo, setGameInfo, fieldTiles, setFieldTiles,
  setShadingTiles, currentTile, unitData, abilityData, menuRef,
  menuSize, movementData, setCurrentTile, winConditionMet,
  setWinConditionMet }) {
  const [actionParams, setActionParams] = useState();
  const [currentPlayer, setCurrentPlayer] = useState()
  const [attackMenu, setAttackMenu] = useState(false)
  const [moveMenu, setMoveMenu] = useState(false)


  let currentTurn = gameInfo.current_turn
  const shade = new ShadeTiles({setShadingTiles: setShadingTiles})

  function changeTurnButton() {
    gameInfo[`player_${playerData.player_num}_int`] += 10;
    setCurrentTile({});
    shade.reset();
    socket.emit("next_turn_out", {port: playerData.port})
  }

  function cancelButton() {
    setAttackMenu(false)
    setMoveMenu(false)
    setActionParams()
    shade.reset()
  }

  function moveButton() {
    setAttackMenu(false)
    setMoveMenu(true)
    setActionParams()
    validTiles({
      action: "march",
      range: movementData[currentTile.archetype]?.march,
      position: currentTile.position,
      setActionParams: setActionParams,
      fieldTiles: fieldTiles,
      setShadingTiles:setShadingTiles,
      gameInfo: gameInfo,
      playerData:playerData,
    })
  };

  function sneakButton() {
    setAttackMenu(false)
    setMoveMenu(true)
    setActionParams()
    validTiles({
      action: "sneak",
      range: movementData[currentTile.archetype]?.sneak,
      position: currentTile.position,
      setActionParams: setActionParams,
      fieldTiles: fieldTiles,
      setShadingTiles:setShadingTiles,
      gameInfo: gameInfo,
      playerData:playerData,
    })
  }

  function attackButton() {
    setMoveMenu(false)
    setAttackMenu(true)
    validTiles({
      action: "attack",
      range: abilityData[currentTile.archetype]?.ability_one_range,
      position: currentTile.position,
      setActionParams: setActionParams,
      fieldTiles: fieldTiles,
      setShadingTiles:setShadingTiles,
      gameInfo: gameInfo,
      playerData:playerData,
    })
  }


  useEffect(() => {
    //  actions params is changed, to fire off the use effect to change initiative
    //  will give data about the action and function for the action to take place
    if (actionParams) {
      if (actionParams.action === "march") {
        moveTo(actionParams.route, setFieldTiles, setShadingTiles, setCurrentTile)
        setGameInfo((current) => {
            let currentUpdate = {...current}
            currentUpdate[`player_${playerData.player_num}_int`] -= movementData[currentTile.archetype]?.march_cost
            return currentUpdate
          })
        setAttackMenu(false)
        setMoveMenu(false)
        setActionParams({})
        }
        else if (actionParams.action === "attack") {
          attackTarget(actionParams, fieldTiles)
          setGameInfo((current) => {
            let currentUpdate = {...current}
            currentUpdate[`player_${playerData.player_num}_int`] -= abilityData[currentTile.archetype]?.ability_one_cost
            return currentUpdate
          })
          WinCondition(fieldTiles, unitData, gameInfo, setWinConditionMet)
          setAttackMenu(false)
          setMoveMenu(false)
          setActionParams({})
          shade.reset()
        }

        else if (actionParams.action === "sneak") {
        moveTo(actionParams.route, setFieldTiles, setShadingTiles, setCurrentTile, actionParams.action)
        setGameInfo((current) => {
            let currentUpdate = {...current}
            currentUpdate[`player_${playerData.player_num}_int`] -= movementData[currentTile.archetype]?.sneak_cost
            return currentUpdate
          })
        setAttackMenu(false)
        setMoveMenu(false)
        setActionParams({})
        }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [actionParams]);

  useEffect(() => {
    let selection = gameInfo[`player_${gameInfo.current_turn}`]
    setCurrentPlayer(selection)
  }, [gameInfo])

  useEffect(() => {
    if (winConditionMet) {
      socket.emit("GAMEOVER", {port: playerData.port, data: winConditionMet})
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [winConditionMet])

  return (

    <div className="container-fluid d-flex flex-row justify-content-between align-items-right p-0 m-0 h-100 border-1" ref={menuRef}>
        <WinModal winConditionMet={winConditionMet} playerData={playerData} />
      <div className="crt pointScreen container-fluid align-items-right" style={{width:menuSize.width, height:menuSize.height}}>
        <div className="container-fluid">
          <div className="container-fluid align-items-right flex-row d-flex">
            <div className="container-fliud">
            {
              <h2>Current Turn: {currentPlayer ? currentPlayer : gameInfo.player_1}</h2>
            }
            <p>Total Turns: {gameInfo.total_turns}</p>
            <p>Total Initiative: {gameInfo[`player_${playerData.player_num}_int`]}</p>
            </div>
            {playerData.player_num === currentTurn ?
            <div className="container-fliud d-flex">
              <button className="crt-btn m-3 h-25 w-75" onClick={changeTurnButton}>Next Turn</button>
            </div> : null
            }
          </div>
        </div>

        {playerData.player_num === currentTurn ? (
        <div className="container-fluid">
          <div className="container-fluid align-items-right p-0 m-0">
            {currentTile && Object.keys(currentTile ? currentTile : {}).length > 0 ? (
              <MenuDetails unitData={unitData} currentTile={currentTile} abilityData={abilityData} attackMenu={attackMenu} moveMenu={moveMenu} movementData={movementData} />
            ) : (
              <p>Select a unit</p>
            )}
          </div>

          {currentTile && Object.keys(currentTile ? currentTile : {}).length > 0 && abilityData[currentTile.archetype]?.ability_one_range > 0 && (
            <>
              {gameInfo[`player_${playerData.player_num}_int`] >= abilityData[currentTile.archetype]?.ability_one_cost ? (
                <>
                  <button className="crt-btn m-1 w-25" onClick={attackButton}>Attack</button>
                </>
              ) : (
                <p>Not enough initiative to Attack</p>
              )}
            </>
          )}
          {currentTile && Object.keys(currentTile ? currentTile : {}).length > 0 && movementData[currentTile.archetype]?.march > 0 && (
            <>
              {gameInfo[`player_${playerData.player_num}_int`] >= movementData[currentTile.archetype]?.march_cost ? (
                <>
                  <button className="crt-btn m-1 w-25" onClick={moveButton}>March</button>
                </>
              ) : (
                <p>Not enough initiative to March</p>
              )}
            </>
          )}
          {currentTile && Object.keys(currentTile ? currentTile : {}).length > 0 && movementData[currentTile.archetype]?.sneak > 0 && (
            <>
              {gameInfo[`player_${playerData.player_num}_int`] >= movementData[currentTile.archetype]?.sneak_cost ? (
                <>
                  <button className="crt-btn m-1 w-25" onClick={sneakButton}>Sneak</button>
                </>
              ) : (
                <p>Not enough initiative to Sneak</p>
              )}
            </>
          )}
          <button className="crt-btn m-1 w-30" onClick={cancelButton}>Cancel</button>
        </div>

        ) : (
        <div>
          {currentTile && Object.keys(currentTile ? currentTile : {}).length > 0 ? (
            <MenuDetails unitData={unitData} currentTile={currentTile} abilityData={abilityData} movementData={movementData} />
          ) : null}
          <p>It is currently {currentPlayer ? currentPlayer : gameInfo.player_1}'s turn</p>
        </div>
        )}
      </div>
    </div>
  );
}

export default CombinedPlayerFunction;
