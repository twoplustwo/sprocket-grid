import { useEffect, useState } from "react"
import useToken from "@galvanize-inc/jwtdown-for-react";
import axios from "axios"


export default function Stats({playerData}) {
    const { token } = useToken()
    const [stats, setStats] = useState()

    useEffect(() => {
        const config = {headers: {Authorization: `Bearer ${token}`}}
        axios.get(`${process.env.REACT_APP_USER_SERVICE_API_HOST}/api/users`, config)
        .then((response) => {
            setStats(response.data)
        })
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    return(
         <div className="container-fluid d-flex flex-column h-100 w-100 p-4">
            <div className="container-fluid table-container h-75">
                <table className="crt-table w-100">
                    <thead className="crt-table-head">
                        <tr>
                            <td>User:</td>
                            <td>Games:</td>
                            <td>Win:</td>
                            <td>Loss:</td>
                            <td>Points:</td>
                        </tr>
                    </thead>

                    <tbody>
                        {
                            stats
                            ?
                            stats.map((stat) => {
                                return(
                                        <tr
                                        key={stat.username}
                                        className="crt-table-row">
                                            <td>{stat.username}</td>
                                            <td>...0</td>
                                            <td>...0</td>
                                            <td>...0</td>
                                            <td>...0</td>
                                        </tr>
                                )
                            })
                            :
                            null
                        }
                    </tbody>
                </table>
            </div>
        </div>
    )
}
