export default function CreateGame({handleCreateSubmit}) {
    return(
        <div className="crt-div container-fluid d-flex flex-column align-items-center w-100 h-100 p-4">
            <h1>Create Game</h1>
            <hr className="crt-border w-75"/>
            <div className="container-fluid d-flex flex-row align-items-center justify-content-center p-0 m-0">
                <label htmlFor="nameInput">Game Name:</label>
                <input type="text" className="crt-input m-1" id="nameInput"></input>

            </div>
            <div className="container-fluid d-flex flex-row align-items-center justify-content-center p-0 m-0">
                <label htmlFor="playerInput">Max Players:</label>
                <input type="text" className="crt-input m-1" id="playerInput"></input>
            </div>
            <button className="crt-btn" onClick={handleCreateSubmit}>Create Game</button>
        </div>
    )
}
