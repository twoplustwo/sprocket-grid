import { useEffect, useState } from "react"
import useToken from "@galvanize-inc/jwtdown-for-react";
import axios from "axios"

export default function ServerList({port, setPort, handleJoinSubmit}) {
    const { token } = useToken()
    const [servers, setServers] = useState()

    useEffect(() => {
        const config = {headers: {Authorization: `Bearer ${token}`}}
        axios.get(`${process.env.REACT_APP_USER_SERVICE_API_HOST}/api/games`, config)
        .then((response) => {
            setServers(response.data.active_games)
        })
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <div className="container-fluid d-flex flex-column h-100 w-100 p-4">

            <div className="container-fluid table-container h-75">

                <table className="crt-table w-100">
                    <thead className="crt-table-head">
                        <tr>
                            <td>Server Name:</td>
                            <td>Port:</td>
                            <td>Player Count:</td>
                        </tr>
                    </thead>

                    <tbody>
                        {
                            servers
                            ?
                            servers.filter((server) => server.total_turns === 0).map((server) => {
                                return(
                                        <tr
                                            key={server.port}
                                            onClick={() => setPort(server.port)}
                                            className={port === server.port ? "crt-table-row-active" : "crt-table-row"}
                                        >
                                            <td>Placeholder Name</td>
                                            <td>{server.port}</td>
                                            <td>{server.player_count}/4</td>
                                        </tr>
                                )
                            })
                            :
                            null
                        }
                    </tbody>
                </table>
            </div>
            {
                port
                ?
                <button className="crt-btn" onClick={handleJoinSubmit}>Join Game</button>
                :
                null

            }
        </div>
    )
}
