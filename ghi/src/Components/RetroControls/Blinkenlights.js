import "../../crt.css"

export function BlkWarning({message, turnOn=false}) {
    return(
        <div className={turnOn ? "blk-warning blk-warning-on" : "blk-warning"} >
            <h4>{message}</h4>
        </div>
    )
}


export function BlkGreen({message, turnOn=false}) {
    return(
         <div className={turnOn ? "blk-green blk-green-on" : "blk-green"} >
            <h4>{message}</h4>
        </div>
    )
}

export function BlkError({message, turnOn=false}) {
    return(
         <div className={turnOn ? "blk-error blk-error-on" : "blk-error"} >
            <h4>{message}</h4>
        </div>
    )
}