import { useState } from 'react'
import useToken from '@galvanize-inc/jwtdown-for-react'
import '../App.css'

export default function CreateAccountModal ({ login, navigate, setPlayerData }) {

    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [username, setUsername] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const { register } = useToken()


    const handleSubmit = (e) => {
        e.preventDefault()
        const userData = {
            first_name: firstName,
            last_name: lastName,
            username: username,
            email: email,
            password: password,
        }
        register(
            userData,
            `${process.env.REACT_APP_USER_SERVICE_API_HOST}/api/accounts`
        )
        setPlayerData((current) => {
            let updatedData = {...current}
            updatedData.username = username
            return updatedData
        })
        e.target.reset()
        navigate("/test")
    }



return (
    <>
    <button type="button" className="nav-link" data-bs-toggle="modal" data-bs-target="#createAccountModal">Create Account</button>
    <div className="modal fade" id="createAccountModal" tabIndex="-1" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content" >
            <div className="shadow bg-accentGrey p-4">
                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <h1 className="modal-title">Create an Account</h1>
            <hr className="line"></hr>
            <form onSubmit={(e) => handleSubmit(e)} >
                <label className="form-label">First Name</label>
                <input name="firstname" type="text" className="form-control" onChange={(e) => setFirstName(e.target.value)} required  />

                <label className="form-label">Last Name</label>
                <input name="lastname"  type="text" className="form-control" onChange={(e) => setLastName(e.target.value)} required  />

                <label className="form-label">Username</label>
                <input name="username" type="text" className="form-control" onChange={(e) => setUsername(e.target.value)} required  />

                <label className="form-label">Email address</label>
                <input name="email" type="email" className="form-control" onChange={(e) => setEmail(e.target.value)} required  />

                <label className="form-label">Password</label>
                <input name="password" type="password" className="form-control" onChange={(e) => setPassword(e.target.value)} required />

            <div className="container d-flex justify-content-center w-100">
            <input id="registerbutton" type="submit" className="btn" value="Register" data-bs-dismiss="modal" />
            </div>
            </form>
            </div>
            </div>
            </div>
        </div>
    </>
)
}
