import { useNavigate  } from "react-router-dom"
import '../App.css'

export default function WinModal ({ winConditionMet, playerData, gameOver}) {

const navigate = useNavigate()

    return (
        <div className={`modal fade ${winConditionMet ? "show d-block" : null}`} tabIndex="-1" id="Modal">
  <div className="modal-dialog">
    <div className="modal-content">
      <div className="shadow bg-accentGrey p-4">
      <div className="modal-header">
        <h5 className="modal-title">GAME OVER</h5>

      </div>
      <div className="modal-body" id="gameovermessage">
         {playerData.username === winConditionMet ? <p>You WIN winner {playerData.username} </p> : <p>You lose loser </p>}
      </div>
      <div className="modal-footer">
        <button onClick={() => navigate("/")} id="gameoverbutton" type="button" className="btn" data-bs-dismiss="Modal">Close</button>
      </div>
    </div>
    </div>
  </div>
</div>
    )
}
