import { useState } from 'react'
import '../App.css'


export default function LoginModal ({ login, navigate, setPlayerData }) {

    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")

    const handleSubmit = (e) => {
        e.preventDefault()
        login(username, password)
        setPlayerData((current) => {
            let updatedData = {...current}
            updatedData.username = username
            return updatedData
        })
        e.target.reset()
        navigate("/")
    }
    return (

        <>
            <button
                type="button"
                className="nav-link"
                data-bs-toggle="modal"
                data-bs-target="#loginModal"
            >
                Login
            </button>
            <div className="modal fade" id="loginModal" tabIndex="-1" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content" id="loginbox" >

                        <div className="shadow bg-accentGrey p-4" >
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            <h1 className="modal-title">Login</h1>
                            <hr className="line"></hr>
                            <form onSubmit={(e) => handleSubmit(e)}>
                                <label className="form-label">Username</label>
                                <input name="username" type="text" className="form-control" onChange={(e) => setUsername(e.target.value)} required  />
                                <label className="form-label">Password</label>
                                <input name="password" type="password" className="form-control" onChange={(e) => setPassword(e.target.value)} required />
                                <div className="container d-flex justify-content-center w-100">
                                    <input id="loginbutton" type="submit"  className="btn" value="Login" data-bs-dismiss="modal"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
