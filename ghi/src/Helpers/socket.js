import { io } from 'socket.io-client';

export const socket = io(`${process.env.REACT_APP_USER_SERVICE_API_HOST}`, {
        path: "/ws/socket.io",
        autoConnect: false
    }
)

export function roomConnect(port, callback=null) {
    const fireCall = (response) => {
        if (response === true && callback !== null) {
            callback()
        }
    }
    if (socket.connected) {
        socket.emit("join_game", port, fireCall)
    }
}
