

export function WinCondition(fieldTiles, unitData, gameInfo, setWinConditionMet) {


const copiedFieldTiles = [...Object.values(fieldTiles)];
    let winner = null

  const matchingArchetypeTiles = [];
  for (const tile of copiedFieldTiles) {
    if (tile.archetype === 8 && tile.health > 0 ) {
      matchingArchetypeTiles.push(tile);
    }
  }

    if ( matchingArchetypeTiles.length === 1){
        if (matchingArchetypeTiles[0].owner === 1) {
            winner = gameInfo.player_1
        } else if  (matchingArchetypeTiles[0].owner === 2) {
            winner = gameInfo.player_2
        } else if (matchingArchetypeTiles[0].owner === 3) {
            winner = gameInfo.player_3
        } else if (matchingArchetypeTiles[0].owner === 4) {
            winner = gameInfo.player_4
        }
        setWinConditionMet(winner)
    }
}
