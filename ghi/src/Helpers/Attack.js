

export function attackTarget(actionParams ,fieldTiles) {
    //recieve current tile and target tile from tile validation
    let target = fieldTiles[actionParams.target]

    //generate random numbers for both
    const numberRoller = (min, max) => {
        return Math.floor(Math.random()
                * (max - min + 1)) + min;
        };
    let targetRoll = numberRoller(1, 20)

    let attackerRoll = numberRoller(1, 20)

    if (attackerRoll >= targetRoll) {
        target.health -= 1
        }

}
