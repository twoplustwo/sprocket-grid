import { ShadeTiles } from "./MapFunctions"


function updatePosition(oldKey, newKey, setState) {

    setState((current) => {
        if (!Boolean(current[newKey])) {
            const newObject = {}
            delete Object.assign(newObject, current, {[newKey]: current[oldKey]})[oldKey]
            return newObject
        } else {
            return current
        }
    })
}



export function moveTo(route, setFieldTiles, setShadingTiles, setCurrentTile, action,) {
        const shade = new ShadeTiles({setShadingTiles: setShadingTiles})
        if (action === "sneak"){
            setFieldTiles((current) => {

                let newField = {...current}
                const owner = newField[route[0]].owner
                let visibleness = {}
                    for (let i=1; i<=4; i++) {
                        if (i === owner) {
                            visibleness[i] = "visible"}
                        else {
                            visibleness[i] = "hidden"
                        }
                    }
                    newField[route[0]].visibility = visibleness
                    console.log("I AM HERE")
                    return newField
                    })}

        else {
            setFieldTiles((current) => {

                let newField = {...current}
                let visibleness = {1: "visible", 2: "visible", 3: "visible", 4: "visible"}
                    newField[route[0]].visibility = visibleness
                    console.log("I AM HERE")
                    return newField
                    })
        }

        updatePosition(route[0], route[route.length-1], setFieldTiles);
        shade.resetAll();
        setCurrentTile({});




    // take current unit position
    // check coords in route for mines
    // if route has mines, stop dead
    //else,
    // update unit position
    return null
}
