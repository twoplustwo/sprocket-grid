
/**
 * converts string coordinates to a numeric array or a numeric array to a string
 * @param {String/Array} cordString
 * @returns Array of numeric coordinates, position[0] = x, position[1] = y if input is string, stringified cords is input numeric array
 */
export function convertCords(cords) {
    if (typeof cords === "string" ){
        return cords.split(",").map(num => Number(num))
    } else {
        return `${cords[0]},${cords[1]}`
    }
}


/**
 * @param {String} oldKey "x,y" The previous key of the object being repositioned
 * @param {String} newKey "x,y" The key replacing the previous
 * @param {Function} setState The setState() method associated with the object being moved
 */
export function updatePosition(oldKey, newKey, setState) {

    setState((current) => {
        if (!Boolean(current[newKey])) {
            const newObject = {}
            delete Object.assign(newObject, current, {[newKey]: current[oldKey] })[oldKey]
            return newObject
        } else {
            return current
        }
    })
}

export class ShadeTiles {
    /**
     *
     * @param {Number} [owner=null]
     * @param {Callback} setShadingTiles
     * @param {Number} [currentTurn=null]
     */
    constructor({owner=null, setShadingTiles, currentTurn=null}) {
        this.owner = owner
        this.setState = setShadingTiles
        this.turn = currentTurn
    }

    /**
     * Sets visibility, color, and the onClick effect of specified shading tiles
     * @param {[{position: "x,y", visibility: Boolean, color: String, turnWhenHidden: Number, onClick: callback}]} cordParams Array of parameters for each tile to shade
     */
    set(cordParams) {
        this.setState((current) => {
            let newObj = {...current}
            cordParams.forEach(element => {
                const visibility = element.isVisible ? "visible" : "hidden"
                let tile = newObj[element.position]

                tile.visibility = visibility
                tile.color = element.color
                tile.turnWhenHidden = element.turnWhenHidden
                tile.owner = this.owner
                tile.onClick = element.onClick
            })
            return newObj
        })
    }

    /**
     * Resets all shading tiles where the value of "turnWhenHidden" < current_turn
     */
    reset() {
        this.setState((current) => {
            let newObj = {...current}
            Object.keys(newObj).forEach((key) => {
                let tile =  newObj[key]
                if (tile.turnWhenHidden != null && tile.turnWhenHidden >= this.turn) {
                    tile.visibility = "visible"
                    // tile.color = tile.color
                    // tile.turnWhenHidden = tile.turnWhenHidden
                    tile.owner = this.owner
                    // tile.onClick = tile.onClick
                } else {
                    tile.visibility = "hidden"
                    tile.color = null
                    tile.turnWhenHidden = null
                    tile.owner = null
                    tile.onClick = null
                }
            })
            return newObj
        })
    }

    /**
     * Resets all tiles where the tile.owner === this.owner
     */
    resetOwner() {
         this.setState((current) => {
            let newObj = {...current}
            Object.keys(newObj).forEach((key) => {
                let tile =  newObj[key]
                if (tile.owner !== this.owner) {
                    // tile.visibility = tile.visibility
                    // tile.color = tile.color
                    // tile.turnWhenHidden = tile.turnWhenHidden
                    tile.owner = this.owner
                    // tile.onClick = tile.onClick
                } else {
                    tile.visibility = "hidden"
                    tile.color = null
                    tile.turnWhenHidden = null
                    tile.owner = null
                    tile.onClick = null
                }
            })
            return newObj
        })
    }
    /**
     * Resets all shading tiles regardless of "turnWhenHidden"
     */
    resetAll() {
        this.setState((current) => {
            let newObj = {...current}
            Object.keys(newObj).forEach((key) => {
                let tile =  newObj[key]

                    tile.visibility = "hidden"
                    tile.color = null
                    tile.turnWhenHidden = null
                    tile.owner = null
                    tile.onClick = null
                }
            )
            return newObj
        })
    }
}
