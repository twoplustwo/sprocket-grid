import { convertCords, ShadeTiles } from "./MapFunctions"

export function validTiles({action, range, position, setActionParams, fieldTiles, setShadingTiles, route=[], gameInfo, playerData}) {

    const shade = new ShadeTiles({
        owner:playerData.player_num,
        setShadingTiles:setShadingTiles,
        currentTurn:gameInfo.current_turn
        }
    )


    const validAction = ["travel", "march", "sneak"]

    if (validAction.includes(action)) {

        if (range > 0) {
            const numericPos = convertCords(position)
            const cordArray = []
            for (let x = (numericPos[0] - 1); x <= (numericPos[0] + 1); x++) {
                for (let y = (numericPos[1] - 1); y <= (numericPos[1] + 1); y++) {
                    if (x >= 0 && x <= 11 + ((gameInfo.player_count - 2) * 3) && y >= 0 && y <= 17) {
                        cordArray.push(convertCords([x, y]))
                    }
                }
            }

            // turn progression needed to test with units able to move/target in range
            const occupiedKeys = Object.keys(fieldTiles).filter(key => {
                const selectedUnit = fieldTiles[key]
                const playerNum = playerData.player_num
                const isVisible = selectedUnit.visibility[playerNum]
                if (isVisible === "visible") {
                    return true
                } else {
                    return false
                }
            })

            const filteredArray = cordArray.filter(tile => !occupiedKeys.includes(tile))

            route.push(position)

            const recursive = (event) => validTiles({
                action: action,
                range: range-1,
                position: event.target.id,
                setActionParams: setActionParams,
                fieldTiles: fieldTiles,
                setShadingTiles: setShadingTiles,
                route: route,
                gameInfo: gameInfo,
                playerData:playerData,
                }
            )

            shade.reset()

            let cordParams = [{"position": route[0], "color": "#fc283a", "isVisible": true, "onClick": null, "turnWhenHidden": 0}]

            filteredArray.forEach((cord) => {
                let new_param = {}
                new_param.position = cord
                new_param.color =  route.includes(cord) ? "#3440eb" : "#90EE90"
                new_param.isVisible = true
                new_param.onClick =  route.includes(cord) ? null : recursive
                new_param.turnWhenHidden =  route.includes(cord) ? gameInfo.current_turn + 4 : null
                cordParams.push(new_param)
            })

            shade.set(cordParams)


        } else {
            route.push(position)

            shade.set([{"position": position, "color": "#5e32a8", "isVisible": true, "onClick": null, "turnWhenHidden": 0}])

            shade.reset()

            const data = {"route": route, "action": action}
            setActionParams(data)

        }

    } else

    if (action === "attack") {
        const convertedCords = convertCords(position)
        const x = convertedCords[0]
        const y = convertedCords[1]
        let excludedRef = []
        let excludedList = []
        let included = []
        const playerNum = playerData.player_num
        const occupiedKeys = Object.keys(fieldTiles).filter(key => {
            const selectedUnit = fieldTiles[key]
            const isVisible = selectedUnit.visibility[playerNum]
            if (isVisible === "visible") {
                return true
            } else {
                return false
            }
        })

        for (let angle = 0; angle <= 360; angle += 1){
            let rad_angle = (angle * (Math.PI / 180))
            let excluded = false
            for (let d = 1; d <= range; d++){
                let new_x = Math.round(x + (d * Math.cos(rad_angle)))
                let new_y = Math.round(y + (d * Math.sin(rad_angle)))
                let stringCords = convertCords([new_x, new_y])

                if (new_x >= 0 && new_x <= 11 + ((gameInfo.player_count - 2) * 3) && new_y >= 0 && new_y <= 17) {
                    if (occupiedKeys.includes(stringCords)) {
                        excludedRef.push(stringCords)
                    }
                    if (excludedRef.includes(stringCords)) {
                        excluded = true
                    }
                    if (excluded && !excludedList.includes(stringCords) && !excludedRef.includes(stringCords)) {
                        excludedList.push(stringCords)
                    }
                    if (!excluded && !included.includes(stringCords)) {
                        included.push(stringCords)
                    }
                }
            }
        }

        let cordParams = []
        included.forEach((cord) => {
            let new_param = {}
            new_param.position = cord
            new_param.color = "#90EE90"
            new_param.isVisible = true
            new_param.onClick = null
            new_param.turnWhenHidden = null
            cordParams.push(new_param)
        })
        excludedRef.forEach((cord) => {
            let new_param = {}
            let data = {"target": cord, "action": action}
            new_param.position = cord
            new_param.isVisible = true
            new_param.turnWhenHidden = null

            if (fieldTiles[cord].owner === playerNum) {
                new_param.color = "#EE9090"
                new_param.onClick = null
            } else {
                new_param.color = "#9090EE"
                new_param.onClick = () => {setActionParams(data)}
            }
            cordParams.push(new_param)
        })
        excludedList.forEach((cord) => {
            let new_param = {}
            new_param.position = cord
            new_param.color = "#EE9090"
            new_param.isVisible = true
            new_param.onClick = null
            new_param.turnWhenHidden = null
            cordParams.push(new_param)
        })

        shade.set(cordParams)

    if (action === "build") {
        const numericPos = convertCords(position)
        const cordArray = []

        for (let x = (numericPos[0] - 1); x <= (numericPos[0] + 1); x++) {
            for (let y = (numericPos[1] - 1); y <= (numericPos[1] + 1); y++) {
                if (x >= 1 && x <= 12 + ((gameInfo.player_count - 2) * 3) && y >= 1 && y <= 18) {
                    cordArray.push(convertCords([x, y]))
                }
            }
        }

        const occupiedKeys = Object.keys(fieldTiles)
        const filteredArray = cordArray.filter(tile => !occupiedKeys.includes(tile))
        const buildFunction = (event) => setActionParams({
            "action": action,
            "position": event.target.id,
        }
        )


        let cordParams = []

        filteredArray.forEach((cord) => {
            let new_param = {}
            new_param.position = cord
            new_param.color = "#90EE90"
            new_param.isVisible = true
            new_param.onClick = buildFunction
            new_param.turnWhenHidden = null
            cordParams.push(new_param)
        })
    }
    } else
    if (action === "sweep") {

    } else
    if (action === "mine") {

    } else
    if (action === "nest") {

    } else
     if (action === "revive") {

    } else {
        console.error()
    }
}
