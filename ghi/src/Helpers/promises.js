
export const assignPlayerData = (setPlayerData, response) => {
    return new Promise((resolve, reject) => {
        try {
            setPlayerData((current) => {
                let updatedData = {...current}
                updatedData.username = response.username
                updatedData.player_num = response.player_num
                updatedData.port = response.port
                updatedData.initiative = response.initiative
                return updatedData
            })
            resolve()
        } catch {
            reject("Failed to find player number!")
        }
    })
}

export const assignMapData = (response, setFloorTiles, setFieldTiles, setShadingTiles) => {
    return new Promise((resolve, reject) => {
        try{
            if (response) {
                setFloorTiles(response.floor_tiles)
                setFieldTiles(response.field_tiles)
                setShadingTiles(response.shade_tiles)
                resolve(response)
            }
            resolve()
        } catch (error) {
            reject(error)
        }
    })
}

export const assignGameInfo = (response, setGameInfo) => {
    return new Promise((resolve, reject) => {
        try{
            if (response) {
                setGameInfo(response)
                resolve(response)
            }
            resolve()
        } catch (error) {
            reject(error)
        }
    })
}
