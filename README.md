# Sprocket Grid

Bang! Welcome to Sprocket Grid, a turn-based, top-down game. Build your lines with troops, fortifications, and buildings, and destroy the enemies Command center before you own falls! With an initiative system, this web game rewards a solid strategy and proper planning to route the enemy troops, and secure victory.

You can find the game deployed at <https://twoplustwo.gitlab.io/sprocket-grid/>

### Developed by:

- Alex Hopkins
- Alvin Liang
- Christopher Byrne
- Matthew Haffke
- Rene Hinojosa

## API Calls: For Mod3 Instructors
- Alex Hopkins
- Alvin Liang
- Christopher Byrne
- Matthew Haffke
- Rene Hinojosa

| name        | Merge Request Title                                         | File name                   | Line number |
| ----------- | ----------------------------------------------------------- | --------------------------- | ----------- |
| Alex        | Multiplayer initialization refactor + home styling          | Pages/GameSetup.js          | 318-331     |
| Alvin       | Frontend Authentication with Login and Create Account pages | Pages/CreateAccount.js      | 16-31       |
| Christopher | string | no        | no      |
| Matt        | Added tables for the 3 layers of map tiles...               | Pages/GameSetup.js          | 340-355     |
| Rene        | Valid tile                                                  | Components/HomeCrt/Stats.js | 10-17       |


## Getting Started

1. Fork this repository
2. Clone the forked repository onto your local computer and inside of your chosen directory:

```
   git clone <https://gitlab.com/twoplustwo/sprocket-grid>
```

3. Build and run the project using Docker with the following commands:

```
docker volume create pg-admin
docker volume create sprocket
docker-compose build
docker-compose up
```

- Make sure all of your docker containers are running

## The Design

- Players can sign up for an account, logout, and login.
- Upon account creation or login, the player will be redirected to the home screen if not already there.
- Players can hit the Create button on the bottom row to bring up the Create Game menu. From here, they can enter a game name and push the Create Game button. This will initialize a game, and send the player to the Game Setup page.
- For players who are looking to join an already created game, they can hit the List button on the bottom. It will show the Server List page, which shows all active games, the name of the server, the port at which the game is located, and the number of players currently assigned to the game. Clicking on a server will show a Join Game button, which up click, will send the player into the game they selected.
- Once inside the Game Setup page, players are shown a grid which shows the players starting space. Below this grid is a list of players in the game, along with a ready up light that shows when the respective player has hit the ready up button. On the right side of the screen, a menu is shown with the port name, the starting initiative for the player, and a carousel with selectable units. These units are to be clicked on, which will show the units name, detection range, and cost in the menu. Players can then hit Purchase, and place the unit on their section of the map. The cost is deducted from the players' initiative. Once all plays ahve hit Ready Up, Player 1 can hit the Start Game button to start the game, and any remaining initiative will be added to the players starting initiative.
- Players are then redirected to the Game Board. This board connects all players through a Websocket connection, so all actions will update on all players screens. Players can then play the game, clicking units and hitting the action they want to take, as long as they have initiative for it. The stats for actions are displayed on a menu next to the Game Board, as is the Username for whose turn it is, and the players own initiative. Players can hit Next turn once finished with a turn, and the next player then gets to go. This continues until only one player has a Command Center left alive, which then displays the winner and closes the game out once all players leave that port.

## Game Rules

Game rules can be found at this google doc- <https://docs.google.com/document/d/1G1xR4m9us76RuLpjrrgVw45CHgTk4jV0fPdLVnOMEgQ/edit?usp=sharing>

## In-Depth Application Design

- [API Design](/docs/api_design.md)
- [Data Model](/docs/data_model.md)
- [Initial Api Design](/docs/APIEndpoints.png)
- [Building Outline](/docs/BuildingOutline.png)
- [Database Structure](/docs/DatabaseStructure.png)
- [Pages Outline 1](/docs/PagesOutline.png)
- [Pages Outline 2](/docs/PagesOutline2.png)
- [Unit Outline](/docs/UnitOutline.png)
